package id.io.mypregnancy.helper;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

public class IntentHelper extends AppCompatActivity {

    public void movePage(Context ctx, Class target) {
        Intent intent = new Intent(ctx, target);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        ctx.startActivity(intent);
    }

}
