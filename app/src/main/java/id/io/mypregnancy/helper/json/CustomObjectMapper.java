package id.io.mypregnancy.helper.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class CustomObjectMapper {

    private static CustomObjectMapper instance;

    private final ObjectMapper objectMapper;

    private CustomObjectMapper() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public static CustomObjectMapper getInstance() {
        if (instance == null) {
            instance = new CustomObjectMapper();
        }
        return instance;

    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
