package id.io.mypregnancy.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

    public static String API_FORMAT = "yyyy-MM-dd";
    public static String ANDROID_FORMAT = "d MMMM yyyy";
    public static String MONTH_FORMAT = "MMM";
    public static String DAY_FORMAT = "d";

    public static String ANDROID_TIME_FORMAT = "hh:mm";
    public static String API_TIME_FORMAT = "hh:mm:ss";

    private static SimpleDateFormat android_format = new SimpleDateFormat(ANDROID_FORMAT);
    private static SimpleDateFormat api_format = new SimpleDateFormat(API_FORMAT);
    private static SimpleDateFormat api_time = new SimpleDateFormat(API_TIME_FORMAT);
    private static SimpleDateFormat android_time = new SimpleDateFormat(ANDROID_TIME_FORMAT);
    private static SimpleDateFormat day_format = new SimpleDateFormat(DAY_FORMAT);
    private static SimpleDateFormat month_format = new SimpleDateFormat(MONTH_FORMAT);


    private DateHelper() {
    }

    public static String convertToAndroid(String date) {
        Date newDate = null;
        try {
            newDate = api_format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return android_format.format(newDate);
    }

    public static String convertToAPI(String date) {
        Date newDate = null;
        try {
            newDate = android_format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return api_format.format(newDate);
    }

    public static String convertToDay(String date) {
        Date newDate = null;
        try {
            newDate = api_format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return day_format.format(newDate);
    }

    public static String convertToMonth(String date) {
        Date newDate = null;
        try {
            newDate = api_format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return month_format.format(newDate);
    }

    public static String convertTimeToAndroid(String time) {
        Date newTime = null;
        try {
            newTime = api_time.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return android_time.format(newTime);
    }

}
