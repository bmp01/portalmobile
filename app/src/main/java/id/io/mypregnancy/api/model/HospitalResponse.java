package id.io.mypregnancy.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id", "name", "address", "phone", "city", "province", "dr-obsgyn-standby",
        "dr-obsgyn-non-standby", "total-doctor", "beds-available"
})
public class HospitalResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("address")
    private String address;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("city")
    private String city;
    @JsonProperty("province")
    private String province;
    @JsonProperty("dr-obsgyn-standby")
    private String drObsgynStandby;
    @JsonProperty("dr-obsgyn-non-standby")
    private String drObsgynNonStandby;
    @JsonProperty("total-doctor")
    private String totalDoctor;
    @JsonProperty("beds-available")
    private String bendAvailable;

    public HospitalResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDrObsgynStandby() {
        return drObsgynStandby;
    }

    public void setDrObsgynStandby(String drObsgynStandby) {
        this.drObsgynStandby = drObsgynStandby;
    }

    public String getDrObsgynNonStandby() {
        return drObsgynNonStandby;
    }

    public void setDrObsgynNonStandby(String drObsgynNonStandby) {
        this.drObsgynNonStandby = drObsgynNonStandby;
    }

    public String getTotalDoctor() {
        return totalDoctor;
    }

    public void setTotalDoctor(String totalDoctor) {
        this.totalDoctor = totalDoctor;
    }

    public String getBendAvailable() {
        return bendAvailable;
    }

    public void setBendAvailable(String bendAvailable) {
        this.bendAvailable = bendAvailable;
    }
}

