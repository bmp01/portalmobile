package id.io.mypregnancy.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id", "name", "gender", "insemination-dt", "hpl-dt", "first-gestation", "been-born",
        "mis-birth", "create-dt"
})
public class UserGestationResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("insemination-dt")
    private String inseminationDt;
    @JsonProperty("hpl-dt")
    private String hplDt;
    @JsonProperty("first-gestation")
    private String firstGestation;
    @JsonProperty("been-born")
    private String beenBorn;
    @JsonProperty("mis-birth")
    private String misBirth;
    @JsonProperty("create-dt")
    private String createDt;

    public UserGestationResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInseminationDt() {
        return inseminationDt;
    }

    public void setInseminationDt(String inseminationDt) {
        this.inseminationDt = inseminationDt;
    }

    public String getHplDt() {
        return hplDt;
    }

    public void setHplDt(String hplDt) {
        this.hplDt = hplDt;
    }

    public String getFirstGestation() {
        return firstGestation;
    }

    public void setFirstGestation(String firstGestation) {
        this.firstGestation = firstGestation;
    }

    public String getBeenBorn() {
        return beenBorn;
    }

    public void setBeenBorn(String beenBorn) {
        this.beenBorn = beenBorn;
    }

    public String getMisBirth() {
        return misBirth;
    }

    public void setMisBirth(String misBirth) {
        this.misBirth = misBirth;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }
}
