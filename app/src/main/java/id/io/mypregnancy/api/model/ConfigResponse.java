package id.io.mypregnancy.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "MOBILE_APP_DESCRIPTION"
})
public class ConfigResponse {

    @JsonProperty("MOBILE_APP_DESCRIPTION")
    private String mobileAppDescription;

    public ConfigResponse() {
    }

    public String getMobileAppDescription() {
        return mobileAppDescription;
    }

    public void setMobileAppDescription(String mobileAppDescription) {
        this.mobileAppDescription = mobileAppDescription;
    }
}

