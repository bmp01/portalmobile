package id.io.mypregnancy.api.util;

public class APIProperty {

    //Base API
    //dummy API
    //public static final String BASE_URL = "http://192.168.1.6:8080";

    //Deployment API
    public static final String BASE_URL = "http://103.247.10.109:8080";
    public static final String SERVICE_PATH = "/portal/system/";

    //@Path Property
    public static final String KEY_USER_ID_PATH = "userId";
    public static final String KEY_VALIDATE_OTP_PATH = "otpToken";

    // COMMON API
    public static final String API_PING_TEST_URL = "api/ping";
    public static final String API_APPLICATION_CONFIG_URL = "api/config/mobile";

    public static final String API_PROVINCE_LIST_URL = "api/locations/province";
    public static final String API_REGENCY_LIST_URL = "api/locations/regency";
    public static final String API_DISTRICT_LIST_URL = "api/locations/district";
    public static final String API_VILLAGE_LIST_URL = "api/locations/village";
    public static final String API_POSTAL_CODE_LIST_URL = "api/locations/postal-code";

    // LOGIN API
    public static final String API_AUTHENTICATION_URL = "api/mobile/login";

    // REGISTRATION API
    public static final String API_REGISTRATION_URL = "api/mobile/register";

    //OTP API
    public static final String API_VALIDATE_OTP_URL = "api/otp/validate/{otpToken}";
    public static final String API_SEND_OTP_URL = "api/otp/send";

    //USER API
    public static final String API_RETRIEVE_USER_URL = "api/users/{userId}";
    public static final String API_UPDATE_USER_URL = "api/users";

    //GESTATION API
    public static final String API_TRACKED_GESTATION_URL = "api/users/gestation/track/{userId}";

    //ADDRESS API
    public static final String API_RETRIEVE_USER_ADDRESS_URL = "api/users/location/{userId}";
    public static final String API_UPDATE_USER_ADDRESS_URL = "api/users/location";

}
