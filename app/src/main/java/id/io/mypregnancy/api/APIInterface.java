package id.io.mypregnancy.api;

import java.util.concurrent.TimeUnit;

import id.io.mypregnancy.api.util.APIProperty;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(APIProperty.BASE_URL + APIProperty.SERVICE_PATH)
            .client(okHttpClient)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_PING_TEST_URL)
    Call<ResponseBody> pingTest();

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_APPLICATION_CONFIG_URL)
    Call<ResponseBody> getApplicationConfig();

    @Headers("Content-Type: application/json")
    @POST(APIProperty.API_AUTHENTICATION_URL)
    Call<ResponseBody> login(@Body String body);

    @Headers("Content-Type: application/json")
    @POST(APIProperty.API_REGISTRATION_URL)
    Call<ResponseBody> register(@Body String body);

    @Headers("Content-Type: application/json")
    @POST(APIProperty.API_VALIDATE_OTP_URL)
    Call<ResponseBody> validateOtp(@Path(APIProperty.KEY_VALIDATE_OTP_PATH) String otpToken);

    @Headers("Content-Type: application/json")
    @POST(APIProperty.API_SEND_OTP_URL)
    Call<ResponseBody> resendOtp(@Body String body);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_RETRIEVE_USER_URL)
    Call<ResponseBody> getUserDetail(@Path(APIProperty.KEY_USER_ID_PATH) String userId);

    @Headers("Content-Type: application/json")
    @PUT(APIProperty.API_UPDATE_USER_URL)
    Call<ResponseBody> updateUser(@Body String body);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_RETRIEVE_USER_ADDRESS_URL)
    Call<ResponseBody> getUserAddress(@Path(APIProperty.KEY_USER_ID_PATH) String userId);

    @Headers("Content-Type: application/json")
    @PUT(APIProperty.API_UPDATE_USER_ADDRESS_URL)
    Call<ResponseBody> updateUserAddress(@Body String body);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_PROVINCE_LIST_URL)
    Call<ResponseBody> getProvinceList();

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_REGENCY_LIST_URL)
    Call<ResponseBody> getRegencyList(@Query("province") String province);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_DISTRICT_LIST_URL)
    Call<ResponseBody> getDistrictList(@Query("regency") String regency);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_VILLAGE_LIST_URL)
    Call<ResponseBody> getVillageList(@Query("district") String district);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_POSTAL_CODE_LIST_URL)
    Call<ResponseBody> getPostalCodeList(@Query("district") String district);

    @Headers("Content-Type: application/json")
    @GET(APIProperty.API_TRACKED_GESTATION_URL)
    Call<ResponseBody> getTrackedGestation(@Path(APIProperty.KEY_USER_ID_PATH) String userId);


    @Headers("Content-Type: application/json")
    @GET("gestation/{userId}")
    Call<ResponseBody> getGestationDetail(@Path("userId") String userId);

    @Headers("Content-Type: application/json")
    @POST("gestation")
    Call<ResponseBody> createGestationDetail(@Body String body);

    @Headers("Content-Type: application/json")
    @PUT("gestation/{id}")
    Call<ResponseBody> updateGestationDetail(@Path("id") String id, @Body String body);

    //schedule resource

    @Headers("Content-Type: application/json")
    @GET("users/schedule/{userId}")
    Call<ResponseBody> getScheduleList(@Path("userId") String userId);

    @Headers("Content-Type: application/json")
    @POST("users/schedule/{userid}")
    Call<ResponseBody> addSchedule(@Path("userid") String userid, @Body String body);

    // emergency resource
    @Headers("Content-Type: application/json")
    @POST("emergency")
    Call<ResponseBody> addEmergencyStatus(@Body String body);

    //doctor resource
    @Headers("Content-Type: application/json")
    @GET("doctors/{id}")
    Call<ResponseBody> getDoctor(@Path("id") String id);

    //hospital resource
    @Headers("Content-Type: application/json")
    @GET("hospitals/queryFilter")
    Call<ResponseBody> getHospitalList(@Query("in") String location);


}
