package id.io.mypregnancy.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OTPRequest {

    @JsonProperty("email")
    private String email;

    public OTPRequest(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
