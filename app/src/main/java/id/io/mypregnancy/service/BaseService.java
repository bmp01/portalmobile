package id.io.mypregnancy.service;

import android.content.Context;
import android.widget.Toast;

import id.io.mypregnancy.helper.IntentHelper;
import id.io.mypregnancy.manager.SessionManager;

public class BaseService {

    private static IntentHelper intentHelper = new IntentHelper();

    protected static void movePage(Context ctx, Class target) {
        intentHelper.movePage(ctx, target);
    }

    protected static SessionManager sessionManager(Context context) {
        SessionManager sessionManager = new SessionManager(context);
        return sessionManager;
    }

    protected static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
