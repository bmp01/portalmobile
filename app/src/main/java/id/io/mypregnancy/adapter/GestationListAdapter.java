package id.io.mypregnancy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.List;

import id.io.mypregnancy.R;
import id.io.mypregnancy.helper.DateHelper;
import id.io.mypregnancy.model.GestationCard;

public class GestationListAdapter extends BaseAdapter {

    Fragment fragment;
    private LayoutInflater inflater;
    private List<GestationCard> gestationCardModelList;

    public GestationListAdapter(Fragment fragment, List<GestationCard> gestationCardModelList) {
        this.fragment = fragment;
        this.gestationCardModelList = gestationCardModelList;
    }

    @Override
    public int getCount() {
        return gestationCardModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return gestationCardModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) fragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cardview_gestation_history, null);
        }

        TextView babyNameCard = (TextView) convertView.findViewById(R.id.babyNameCard);
        TextView babyGenderCard = (TextView) convertView.findViewById(R.id.babyGenderCard);
        TextView babyHplCard = (TextView) convertView.findViewById(R.id.babyHplCard);
        TextView status = (TextView) convertView.findViewById(R.id.status);
        TextView messageText = (TextView) convertView.findViewById(R.id.messageText);

        LinearLayout list_gestation = (LinearLayout) convertView.findViewById(R.id.list_gestation);


        messageText.setVisibility(View.GONE);

        if (!gestationCardModelList.isEmpty()) {
            babyNameCard.setVisibility(View.VISIBLE);
            babyGenderCard.setVisibility(View.VISIBLE);
            babyHplCard.setVisibility(View.VISIBLE);
            status.setVisibility(View.VISIBLE);
            messageText.setVisibility(View.GONE);

            GestationCard data = gestationCardModelList.get(position);
            babyNameCard.setText(data.getBabyName());
            babyGenderCard.setText(data.getBabyGender());
            String date = DateHelper.convertToAndroid(data.getBabyHpl());
            babyHplCard.setText("HPL : " + date);
            if (data.getStatus().equals("In progress")) {
                status.setBackground(ContextCompat.getDrawable(convertView.getContext(), R.drawable.style_button_green));
                status.setText(data.getStatus());
            } else {
                status.setText(data.getStatus());
                status.setBackground(ContextCompat.getDrawable(convertView.getContext(), R.drawable.style_button_red));
            }

        } else {
            babyNameCard.setVisibility(View.GONE);
            babyGenderCard.setVisibility(View.GONE);
            status.setVisibility(View.GONE);
            babyHplCard.setVisibility(View.GONE);
            messageText.setVisibility(View.VISIBLE);
            messageText.setText("Tidak ada riwayat kehamilan");
        }

        return convertView;
    }
}
