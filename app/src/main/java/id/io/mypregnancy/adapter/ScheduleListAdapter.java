package id.io.mypregnancy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.List;

import id.io.mypregnancy.R;
import id.io.mypregnancy.model.ScheduleCard;

public class ScheduleListAdapter extends BaseAdapter {

    Fragment fragment;
    private LayoutInflater inflater;
    private List<ScheduleCard> scheduleCardList;

    public ScheduleListAdapter(Fragment fragment, List<ScheduleCard> scheduleCardList) {
        this.fragment = fragment;
        this.scheduleCardList = scheduleCardList;
    }

    @Override
    public int getCount() {
        return scheduleCardList.size();
    }

    @Override
    public Object getItem(int position) {
        return scheduleCardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) fragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cardview_user_schedule, null);
        }

        TextView scheduleName = (TextView) convertView.findViewById(R.id.scheduleName);
        TextView appointmentTime = (TextView) convertView.findViewById(R.id.appointmentTime);
        TextView appointmentDate = (TextView) convertView.findViewById(R.id.appointmentDate);
        TextView status = (TextView) convertView.findViewById(R.id.status);

        LinearLayout scheduleList = (LinearLayout) convertView.findViewById(R.id.scheduleList);

        if (!scheduleCardList.isEmpty()) {
            scheduleName.setVisibility(View.VISIBLE);
            appointmentTime.setVisibility(View.VISIBLE);
            appointmentDate.setVisibility(View.VISIBLE);
            status.setVisibility(View.VISIBLE);

            ScheduleCard data = scheduleCardList.get(position);
            scheduleName.setText(data.getScheduleName());
            appointmentTime.setText(data.getAppointmentTime());
            appointmentDate.setText(data.getAppointmentDate());

            if (data.getStatus().equals("in progress")) {
                status.setBackground(ContextCompat.getDrawable(convertView.getContext(), R.drawable.style_circle_green));
                status.setText(data.getStatus());
            } else if (data.getStatus().equals("done")) {
                status.setText(data.getStatus());
                status.setBackground(ContextCompat.getDrawable(convertView.getContext(), R.drawable.style_circle_blue));
            } else {
                status.setText(data.getStatus());
                status.setBackground(ContextCompat.getDrawable(convertView.getContext(), R.drawable.style_circle_red));
            }

        } else {
            scheduleName.setVisibility(View.GONE);
            appointmentTime.setVisibility(View.GONE);
            appointmentDate.setVisibility(View.GONE);
            status.setVisibility(View.GONE);
        }

        return convertView;
    }
}
