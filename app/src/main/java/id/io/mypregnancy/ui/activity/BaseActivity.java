package id.io.mypregnancy.ui.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.io.mypregnancy.helper.IntentHelper;
import id.io.mypregnancy.manager.SessionManager;

public class BaseActivity extends AppCompatActivity {

    private static IntentHelper intentHelper = new IntentHelper();

    protected static void movePage(Context ctx, Class target) {
        intentHelper.movePage(ctx, target);
    }

    protected static SessionManager sessionManager(Context context) {
        SessionManager sessionManager = new SessionManager(context);
        return sessionManager;
    }

    protected static void showMessage(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }

    protected static boolean isOnline(Context ctx) {
        ConnectivityManager conMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }
}
