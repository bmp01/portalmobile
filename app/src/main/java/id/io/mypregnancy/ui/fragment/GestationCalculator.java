package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.io.mypregnancy.R;
import id.io.mypregnancy.constant.ConstantHelper;

public class GestationCalculator extends BaseFragment {

    View root;

    CalendarView hplCalendar;
    Button countHpl;

    String date;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_gestation_calculator, container, false);

        initElement();

        hplCalendar.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            hplDate = year + "-" + (month + 1) + "-" + dayOfMonth;
            date = hplDate;
        });
        countHpl.setOnClickListener(v -> {

            JSONObject sessionJson = new JSONObject();
            try {
                sessionManager(root.getContext());

                sessionJson.put(ConstantHelper.KEY_GESTATION_INSEMINATION, date);

                sessionJson.put(ConstantHelper.KEY_GESTATION_HPL, String.valueOf(calculateDueDate(date)));
                //SessionManager.setGestationSession(sessionJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            moveFragment(GestationCalculator.this, new UserGestationFragment());
        });

        return root;
    }

    public void initElement() {
        hplCalendar = root.findViewById(R.id.hplCalendar);
        countHpl = root.findViewById(R.id.countHpl);
    }

    public String calculateDueDate(String hplDate) {
        String dueDate = null;
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// HH:mm:ss");
            calendar.setTime(df.parse(hplDate));
            calendar.add(Calendar.DATE, 280);  // number of days to add
            dueDate = df.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dueDate;
    }
}
