package id.io.mypregnancy.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.constant.MessageConstant;
import id.io.mypregnancy.helper.DateHelper;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.User;
import id.io.mypregnancy.model.UserGestation;
import id.io.mypregnancy.ui.custom.CustomScrollView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment {

    private View root;

    TextView txtTodayDate, txtDayTime, txtUserFullName, txtDays, txtWeeks, txtPregnantPeriod, gestationPeriodDescription, txtBornEstimateMonths, txtBornEstimateDays, txtBornEstimateText;
    ProgressBar gestationProgress, loading_progress;
    RelativeLayout gestationPeriodSection, gestationEstimateSection, schedulesAction, dailyNotesAction, personalTaskAction, loading_pages;
    CustomScrollView homeScroll;
    //helpers
    Calendar calendar;
    Map<String, String> sessionMap = new HashMap<>();

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        //remove action bar
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        init();

        sessionManager(root.getContext());
        if (!SessionManager.hasLogin()) {
            SessionManager.checkLoginSession();
        }

        try {
            Map<String, String> sessionJson = SessionManager.getLoginSession();

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Calendar currentTimeNow = Calendar.getInstance();

            if (!SessionManager.hasRefreshTime()) {
                currentTimeNow.add(Calendar.MINUTE, ConstantHelper.SESSION_EXPIRY);
                SessionManager.setRefreshTime(dateFormat.format(currentTimeNow.getTime()));
            }

            renewSession();
            getUserDetails(sessionJson.get(ConstantHelper.KEY_USERID));

        } catch (ParseException e) {
            showMessage(root.getContext(), e.getMessage());
        }

        schedulesAction.setOnClickListener(v -> {
            moveFragment(HomeFragment.this, new UserSchedulesFragment());
        });
        dailyNotesAction.setOnClickListener(v -> {
            moveFragment(HomeFragment.this, new UserDailyNotesFragment());
        });
        personalTaskAction.setOnClickListener(v -> {
            moveFragment(HomeFragment.this, new UserTaskFragment());
        });
        gestationEstimateSection.setOnClickListener(V -> {
            moveFragment(HomeFragment.this, new UserGestationFragment());
        });
        gestationPeriodSection.setOnClickListener(V -> {
            moveFragment(HomeFragment.this, new UserGestationFragment());
        });

        return root;
    }

    public void getUserDetails(String userId) throws ParseException {

        if (SessionManager.isUserSession()) {
            loadPage(true, loading_progress, homeScroll, loading_pages);
            User userSession = SessionManager.getUserSession();
            Map<String, String> userMap = parseUserToMap(userSession);

            UserGestation gestation = SessionManager.getGestationSession();
            Map<String, String> gestationMap = parseGestationToTrackingMap(gestation);

            sessionMap.clear();
            sessionMap.putAll(userMap);
            sessionMap.putAll(gestationMap);

            mapContent(sessionMap);

        } else {
            loadPage(true, loading_progress, homeScroll, loading_pages);

            APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
            Call<ResponseBody> userDetailCall = apiInterface.getUserDetail(userId);

            userDetailCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> userCall, Response<ResponseBody> response) {
                    try {
                        if (response.code() == 200) {
                            String json = response.body().string();
                            User user = JsonHelper.fromJson(json, User.class);
                            SessionManager.setUserSession(user);

                            Map<String, String> userMap = parseUserToMap(user);
                            sessionMap.putAll(userMap);

                            APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
                            Call<ResponseBody> trackedGestationCall = apiInterface.getTrackedGestation(userId);
                            trackedGestationCall.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> trackedCall, Response<ResponseBody> response) {
                                    if (response.code() == 200) {
                                        try {
                                            String json = response.body().string();
                                            UserGestation gestationResponse = JsonHelper.fromJson(json, UserGestation.class);
                                            SessionManager.setGestationSession(gestationResponse);

                                            Map<String, String> gestationMap = parseGestationToTrackingMap(gestationResponse);
                                            sessionMap.putAll(gestationMap);

                                        } catch (IOException e) {
                                            loadPage(false, loading_progress, homeScroll, loading_pages);
                                            showMessage(root.getContext(), e.getMessage());
                                        }
                                    }
                                    try {
                                        mapContent(sessionMap);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                                    loadPage(false, loading_progress, homeScroll, loading_pages);
                                    showMessage(root.getContext(), t.getMessage());
                                }
                            });

                        }
                    } catch (IOException e) {
                        loadPage(false, loading_progress, homeScroll, loading_pages);
                        showMessage(root.getContext(), e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                    loadPage(false, loading_progress, homeScroll, loading_pages);
                    showMessage(root.getContext(), t.getMessage());
                }
            });
            loadPage(false, loading_progress, homeScroll, loading_pages);
        }
    }

    public void showAddGestationDialog(Map<String, String> contentMap) {
        if (contentMap.get(ConstantHelper.KEY_GESTATION_ID) == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(root.getContext());
            builder.setMessage("Tidak daftar pantau kehamilan");
            builder.setCancelable(true);
            builder.setPositiveButton(
                    "Tambah",
                    (dialog, id) -> moveFragment(HomeFragment.this, new UserGestationFragment()));
            builder.setNegativeButton(
                    "Tutup",
                    (dialog, id) -> dialog.cancel());
            AlertDialog allert = builder.create();
            allert.show();
        }
    }

    public void mapContent(Map<String, String> contentMap) throws ParseException {
        calendar = Calendar.getInstance();
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd MMMM");
        Date currentDt = calendar.getTime();
        txtTodayDate.setText(new SimpleDateFormat("EEEE", Locale.getDefault()).format(currentDt.getTime()) + ", " + dayFormat.format(calendar.getTime()));
        txtDayTime.setText(greetings(calendar.get(Calendar.HOUR_OF_DAY)));
        txtUserFullName.setText(contentMap.get(ConstantHelper.KEY_FULLNAME));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (contentMap.get(ConstantHelper.KEY_GESTATION_ID) != null) {
            Date hplDate = dateFormat.parse(contentMap.get(ConstantHelper.KEY_GESTATION_INSEMINATION));
            txtWeeks.setText(String.valueOf(calculateWeekNo(hplDate)));
            txtDays.setText(String.valueOf(calculateDayNo(hplDate)));
            txtPregnantPeriod.setText(calculateSemester(calculateWeekNo(hplDate)));
            gestationProgress.setMax(100);
            String gestationDescription = MessageConstant.MESSAGE_GESTATION_DESCRIPTION;
            gestationDescription = gestationDescription.replaceAll("%weeks%", String.valueOf(calculateWeekNo(hplDate))).replaceAll("%days%", String.valueOf(calculateDayNo(hplDate)));
            gestationPeriodDescription.setText(gestationDescription);
            gestationProgress.setProgress(countProgress(calculateDayNo(hplDate)));
            String dueDate = contentMap.get(ConstantHelper.KEY_GESTATION_HPL);
            String resultDate = DateHelper.convertToAndroid(dueDate);
            String gestationEstimationText = MessageConstant.MESSAGE_GESTATION_DUE_DATE;
            gestationEstimationText = gestationEstimationText.replaceAll("%dueDate%", resultDate);
            txtBornEstimateText.setText(gestationEstimationText);
            String day = DateHelper.convertToDay(dueDate);
            String month = DateHelper.convertToMonth(dueDate);
            txtBornEstimateDays.setText(day);
            txtBornEstimateMonths.setText(month);
        } else {
            showAddGestationDialog(sessionMap);
        }
        loadPage(false, loading_progress, homeScroll, loading_pages);
    }

    public String greetings(int hour) {

        String greeting = null;
        if (hour >= 12 && hour < 17) {
            greeting = "Selamat siang,";
        } else if (hour >= 17 && hour < 21) {
            greeting = "Selamat sore,";
        } else if (hour >= 21 && hour < 24) {
            greeting = "Selamat malam,";
        } else {
            greeting = "Selamat pagi,";
        }
        return greeting;
    }

    public String calculateSemester(int weeks) {
        String result = null;
        if (weeks > 28) {
            result = "Trisemester ketiga";
        } else if (weeks == 14 && weeks > 27) {
            result = "Trisemester kedua";
        } else {
            result = "Trisemester pertama";
        }
        return result;
    }

    public int calculateWeekNo(Date start) {
        Locale locale = new Locale("id", "ID");
        Calendar cal = new GregorianCalendar(locale);
        cal.setTime(start);
        int weeks = 0;
        while (cal.getTime().before(Calendar.getInstance().getTime())) {
            cal.add(Calendar.WEEK_OF_YEAR, 1);
            weeks++;
        }
        return weeks;
    }

    public int calculateDayNo(Date start) {
        Locale locale = new Locale("id", "ID");
        Calendar cal = new GregorianCalendar(locale);
        cal.setTime(start);
        int weeks = 0;
        while (cal.getTime().before(Calendar.getInstance().getTime())) {
            cal.add(Calendar.DAY_OF_YEAR, 1);
            weeks++;
        }
        return weeks;
    }

    public int countProgress(int progress) {
        int result = (progress * 100) / 280;
        return result;
    }

    public void init() {
        // initiate todayDateSection elements
        txtTodayDate = root.findViewById(R.id.txtTodayDate);
        // initiate greetingsSection elements
        txtDayTime = root.findViewById(R.id.txtDayTime);
        txtUserFullName = root.findViewById(R.id.txtUserFullName);
        txtDays = root.findViewById(R.id.txtDays);
        txtWeeks = root.findViewById(R.id.txtWeeks);
        // initiate gestationPeriodSection elements
        txtPregnantPeriod = root.findViewById(R.id.txtPregnantPeriod);
        gestationPeriodDescription = root.findViewById(R.id.gestationPeriodDescription);
        gestationProgress = root.findViewById(R.id.gestationProgress);
        gestationPeriodSection = root.findViewById(R.id.gestationPeriodSection);
        //gestationEstimateSection elements
        txtBornEstimateMonths = root.findViewById(R.id.txtBornEstimateMonths);
        txtBornEstimateDays = root.findViewById(R.id.txtBornEstimateDays);
        txtBornEstimateText = root.findViewById(R.id.txtBornEstimateText);
        gestationEstimateSection = root.findViewById(R.id.gestationEstimateSection);
        //actionSection elements
        schedulesAction = root.findViewById(R.id.schedulesAction);
        dailyNotesAction = root.findViewById(R.id.dailyNotesAction);
        personalTaskAction = root.findViewById(R.id.personalTaskAction);
        // initiate commonsPage elements
        loading_progress = root.findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);
        homeScroll = root.findViewById(R.id.homeScroll);
        loading_pages = root.findViewById(R.id.loading_pages);
    }
}
