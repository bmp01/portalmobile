package id.io.mypregnancy.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import id.io.mypregnancy.R;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.UserAddress;

public class GetCurrentLocationFragment extends BaseFragment implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback {

    //root element
    View root;

    // ui element
    SupportMapFragment mapFragment;
    Button btnSetLocation;

    LocationManager locationManager;
    LocationListener locationListener;
    UiSettings mUiSettings;
    Location location;
    GoogleMap mMap;

    public void initElement() {
        btnSetLocation = root.findViewById(R.id.btnSetLocation);
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_getcurrent_location, container, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.userLocationMap);
        mapFragment.getMapAsync(this);

        initElement();


        btnSetLocation.setOnClickListener(v -> {

            sessionManager(root.getContext());
            UserAddress addressCache = SessionManager.getUserAddressCacheSession();
            addressCache.setLatitude(String.valueOf(location.getLatitude()));
            addressCache.setLongitude(String.valueOf(location.getLongitude()));
            SessionManager.removeUserAddressCacheSession();
            SessionManager.setUserAddressCacheSession(addressCache);

            moveFragment(GetCurrentLocationFragment.this, new AccountUserAddressFragment());

        });


        return root;
    }

    public void centreMapOnLocation(Location location, String title) {

        LatLng userLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(userLocation).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                centreMapOnLocation(lastKnownLocation, "Your Location");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);


        mUiSettings = mMap.getUiSettings();

        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setMapToolbarEnabled(true);

        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);


        Intent intent = getActivity().getIntent();
        if (intent.getIntExtra("Place Number", 0) == 0) {

            // Zoom into users location
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };
            getLocation();


        }
    }

    private void getLocation() {
        if (ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            //Toast.makeText(getApplicationContext(), lastKnownLocation., Toast.LENGTH_SHORT).show();
            centreMapOnLocation(location, "Your Location");

        }
    }

    @Override
    public boolean onMyLocationButtonClick() {

        getLocation();
        centreMapOnLocation(location, "My Location");

        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

        getLocation();
        centreMapOnLocation(location, "My Location");

    }

    @Override
    public void onResume() {
        GooglePlayServicesUtil.isGooglePlayServicesAvailable(root.getContext());
        super.onResume();
    }

}