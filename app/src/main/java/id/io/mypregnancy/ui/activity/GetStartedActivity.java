package id.io.mypregnancy.ui.activity;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.api.model.ConfigResponse;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetStartedActivity extends BaseActivity {

    Button btnLogin, btnRegister;
    TextView application_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_getstarted);
        init();
        getApplicationConfig();

        sessionManager(getApplicationContext());

        if (!SessionManager.hasLogin()) {
            SessionManager.checkLoginSession();
        } else {
            movePage(GetStartedActivity.this, MainActivity.class);
        }

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(v ->
                movePage(GetStartedActivity.this, LoginActivity.class));
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(v ->
                movePage(GetStartedActivity.this, RegisterActivity.class));
    }

    public void getApplicationConfig() {
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.getApplicationConfig();
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> configCall, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String json = response.body().string();
                        ConfigResponse configResponse = JsonHelper.fromJson(json, ConfigResponse.class);
                        application_description.setText(configResponse.getMobileAppDescription());

                    } catch (IOException e) {
                        showMessage(getApplicationContext(), e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getApplicationContext(), t.getMessage());
            }
        });

    }

    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void init() {
        application_description = (TextView) findViewById(R.id.application_description);
    }
}
