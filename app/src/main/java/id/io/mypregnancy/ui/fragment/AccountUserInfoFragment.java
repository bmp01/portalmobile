package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.text.ParseException;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.User;
import id.io.mypregnancy.ui.custom.CustomScrollView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountUserInfoFragment extends BaseFragment {

    // Root elements
    View root;

    EditText userName, givenName, userEmail, cardId, userPhone;
    Spinner userAge, userType;

    Button btnAction;

    ProgressBar loading_progress;
    CustomScrollView homeScroll;
    RelativeLayout loadingSection;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_account_userinfo, container, false);
        init();
        generateAgeValue("0");
        generateRelationValue("lainnya");

        String USERID = "";
        try {
            loadPage(true, loading_progress, homeScroll, loadingSection);
            renewSession();
            if (!SessionManager.isUserSession()) {
                getUserDetails(SessionManager.getUserIdSession());
            } else {
                User user = SessionManager.getUserSession();
                USERID = user.getUserId();
                mapContent(user);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        String finalUSERID = USERID;
        btnAction.setOnClickListener(v -> {

            if (btnAction.getText().equals("Simpan")) {

                if (userName.getText().toString().trim().equals("")) {
                    userName.setError("Required");
                    userName.setHint("masukkan nama lengkap");
                    actionStatus(true);
                } else if (givenName.getText().toString().trim().equals("")) {
                    givenName.setError("Required");
                    givenName.setHint("masukkan nama panggilan");
                    actionStatus(true);
                } else if (cardId.getText().toString().trim().equals("")) {
                    cardId.setError("Required");
                    cardId.setHint("masukkan no KTP");
                    actionStatus(true);
                } else if (userPhone.getText().toString().trim().equals("")) {
                    userPhone.setError("Required");
                    userPhone.setHint("masukkan no telepon");
                    actionStatus(true);
                } else {
                    User user = new User();
                    user.setUserId(finalUSERID);
                    user.setCardId(cardId.getText().toString());
                    user.setFullName(userName.getText().toString());
                    user.setGivenName(givenName.getText().toString());
                    user.setEmail(userEmail.getText().toString());
                    user.setPhone(userPhone.getText().toString());

                    String age = "";
                    if (userAge.getSelectedItem().equals("Dibawah 21 tahun")) {
                        age = "0";
                    } else {
                        age = userAge.getSelectedItem().toString();
                    }
                    user.setAge(age);
                    user.setRelation(userType.getSelectedItem().toString());

                    updateContent(user);
                    actionStatus(false);
                }

            } else if (btnAction.getText().equals("Ubah")) {
                actionStatus(true);
            }
        });

        return root;
    }

    private void init() {
        userName = root.findViewById(R.id.userName);
        givenName = root.findViewById(R.id.givenName);
        userEmail = root.findViewById(R.id.userEmail);
        cardId = root.findViewById(R.id.cardId);
        userPhone = root.findViewById(R.id.userPhone);
        userAge = root.findViewById(R.id.userAge);
        userType = root.findViewById(R.id.userType);
        btnAction = root.findViewById(R.id.btnAction);

        if (btnAction.getText().equals("Ubah")) {
            disableElement(true);
        }

        loading_progress = root.findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);
        homeScroll = root.findViewById(R.id.homeScroll);
        loadingSection = root.findViewById(R.id.loadingSection);
    }

    private void disableElement(boolean enable) {
        if (enable) {
            userName.setEnabled(false);
            givenName.setEnabled(false);
            cardId.setEnabled(false);
            userPhone.setEnabled(false);
            userAge.setEnabled(false);
            userType.setEnabled(false);

        } else {
            userName.setEnabled(true);
            givenName.setEnabled(true);
            cardId.setEnabled(true);
            userPhone.setEnabled(true);
            userAge.setEnabled(true);
            userType.setEnabled(true);
        }
    }

    private void actionStatus(boolean status) {
        if (status) {
            disableElement(false);
            btnAction.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.style_button_green));
            btnAction.setText("Simpan");
        } else {
            btnAction.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.style_button_blue));
            disableElement(true);
            btnAction.setText("Ubah");
        }
    }

    private void mapContent(User content) {

        userName.setText(content.getFullName());
        givenName.setText(content.getGivenName());
        userEmail.setText(content.getEmail());
        cardId.setText(content.getCardId());
        userPhone.setText(content.getPhone());
        generateAgeValue(content.getAge());
        generateRelationValue(content.getRelation());

        loadPage(false, loading_progress, homeScroll, loadingSection);

    }

    private void generateAgeValue(String age) {

        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.ages);
        userAge.setAdapter(adapter);
        int ages = Integer.parseInt(age);
        if (ages < 21) {
            int spinnerPosition = adapter.getPosition("Dibawah 21 tahun");
            userAge.setSelection(spinnerPosition);
        } else if (ages >= 21) {
            int spinnerPosition = adapter.getPosition(age);
            userAge.setSelection(spinnerPosition);
        } else if (ages > 40) {
            int spinnerPosition = adapter.getPosition("40+");
            userAge.setSelection(spinnerPosition);
        }
    }

    private void generateRelationValue(String relation) {
        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.relations);

        userType.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition(relation);
        userType.setSelection(spinnerPosition);

    }

    private void updateContent(User content) {
        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        String json = JsonHelper.toJson(content);
        Call<ResponseBody> call = apiInterface.updateUser(json);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    showMessage(root.getContext(), "Perubahan disimpan");
                    getUserDetails(content.getUserId());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadPage(false, loading_progress, homeScroll, loadingSection);
                showMessage(root.getContext(), t.getMessage());
            }
        });

    }

    public void getUserDetails(String userId) {

        loadPage(true, loading_progress, homeScroll, loadingSection);

        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> userDetailCall = apiInterface.getUserDetail(userId);

        userDetailCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> userCall, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        SessionManager.removeUserSession();
                        String json = response.body().string();
                        User user = JsonHelper.fromJson(json, User.class);
                        SessionManager.setUserSession(user);

                        mapContent(user);
                        loadPage(false, loading_progress, homeScroll, loadingSection);
                    }
                } catch (IOException e) {
                    loadPage(false, loading_progress, homeScroll, loadingSection);
                    showMessage(root.getContext(), e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                loadPage(false, loading_progress, homeScroll, loadingSection);
                showMessage(root.getContext(), t.getMessage());
            }
        });
    }
}
