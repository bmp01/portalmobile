package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import id.io.mypregnancy.R;
import id.io.mypregnancy.adapter.GestationListAdapter;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.helper.DateHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.Gestation;
import id.io.mypregnancy.model.GestationCard;
import id.io.mypregnancy.ui.custom.CustomScrollView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserGestationFragment extends BaseFragment {

    // root elements
    View root;

    //Lazy load Section
    ProgressBar loading_progress;
    CustomScrollView homeScroll;
    RelativeLayout loadingSection;

    //Gestation section elemets
    TextView editBaby;
    EditText babyName;
    Spinner babyGender;
    EditText babyHpl;
    Button calculateHpl;
    Switch fistGestationSwitch;
    Switch beenBornSwitch;
    Switch missBirthSwitch;

    //historySection elements
    LinearLayout list_gestation;

    //helper elemets
    GestationListAdapter gestationListAdapter;
    List<GestationCard> gestationCardModels = new ArrayList<>();

    String userid;
    int gestatinId;
    String inseminationDate;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_user_gestation, container, false);

        /*initElement();
        loadPage(false);

        generateBabyGenderValue("Tidak diketahui");
        sessionManager(root.getContext());

        userid = SessionManager.getLoginSession().get(ConstantHelper.KEY_USERID);


        if (SessionManager.isGestationSession()) {
            JSONObject gestationSession = SessionManager.getGestationSession();

            try {
                inseminationDate = gestationSession.getString(ConstantHelper.KEY_GESTATION_INSEMINATION);
                babyName.setText(gestationSession.getString(ConstantHelper.KEY_GESTATION_NAME));
                generateBabyGenderValue(gestationSession.getString(ConstantHelper.KEY_GESTATION_GENDER));
                String date = DateHelper.convertToAndroid(gestationSession.getString(ConstantHelper.KEY_GESTATION_HPL));
                babyHpl.setText(date);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        getGestationDetails(userid);

        editBaby.setOnClickListener(v -> {
            String state = editBaby.getText().toString();
            if (state.contains("Ubah")) {
                babyName.setEnabled(true);
                babyGender.setEnabled(true);
                calculateHpl.setEnabled(true);
                fistGestationSwitch.setEnabled(true);
                beenBornSwitch.setEnabled(true);
                missBirthSwitch.setEnabled(true);

                editBaby.setText("Simpan");
                editBaby.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_green));

            } else if (state.contains("Simpan")) {
                try {
                    Gestation editedGestation = new Gestation();
                    editedGestation.setId(gestatinId);
                    editedGestation.setBabyName(babyName.getText().toString());
                    editedGestation.setBabyGender(babyGender.getSelectedItem().toString());

                    String date = DateHelper.convertToAPI(babyHpl.getText().toString());
                    editedGestation.setDueDate(date);

                    editedGestation.setInsemination(inseminationDate);
                    editedGestation.setFirstPregnant(fistGestationSwitch.isChecked());
                    editedGestation.setBeenBorn(beenBornSwitch.isChecked());
                    editedGestation.setMissBirth(missBirthSwitch.isChecked());

                    updateGestation(gestatinId, editedGestation);

                    showMessage(root.getContext(), "Detail kehamilan di tersimpan");
                    editBaby.setText("Ubah");
                    editBaby.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_blue));
                    disableElements(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (state.contains("Tambah")) {


                try {
                    if (babyHpl.getText().toString().isEmpty()) {

                        showMessage(root.getContext(), "HPL Belum di hitung");

                    } else {
                        Gestation addGestation = new Gestation();
                        addGestation.setBabyName(babyName.getText().toString());
                        addGestation.setBabyGender(babyGender.getSelectedItem().toString());

                        String date = DateHelper.convertToAPI(babyHpl.getText().toString());
                        addGestation.setDueDate(date);

                        addGestation.setInsemination(inseminationDate);
                        addGestation.setFirstPregnant(fistGestationSwitch.isChecked());
                        addGestation.setBeenBorn(beenBornSwitch.isChecked());
                        addGestation.setMissBirth(missBirthSwitch.isChecked());

                        addGestationDetails(userid, addGestation);

                        showMessage(root.getContext(), "Detail kehamilan di tersimpan");
                        editBaby.setText("Ubah");
                        editBaby.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_blue));
                        disableElements(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });

        calculateHpl.setOnClickListener(v -> {

            JSONObject sessionJson = new JSONObject();
            try {
                sessionJson.put(ConstantHelper.KEY_GESTATION_NAME, babyName.getText().toString());
                sessionJson.put(ConstantHelper.KEY_GESTATION_GENDER, babyGender.getSelectedItem().toString());

                SessionManager.setGestationSession(sessionJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            moveFragment(UserGestationFragment.this, new GestationCalculator());
        });
*/
        return root;
    }

    public void getGestationDetails(String userId) {
        loadPage(true);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.getGestationDetail(userId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.code() == 200) {

                        JSONArray responseArray = new JSONArray(response.body().string());
                        List<Gestation> gestationList = new ArrayList<>();

                        if (responseArray.length() == 0) {
                            editBaby.setText("Tambah");
                            calculateHpl.setText("Hitung HPL");

                            babyName.setEnabled(true);
                            babyGender.setEnabled(true);
                            calculateHpl.setEnabled(true);
                            fistGestationSwitch.setEnabled(true);
                            beenBornSwitch.setEnabled(true);
                            missBirthSwitch.setEnabled(true);

                        } else {
                            for (int i = 0; i < responseArray.length(); i++) {
                                Gestation gestation = new Gestation();
                                GestationCard gestationCard = new GestationCard();

                                gestation.setId(responseArray.getJSONObject(i).getInt(ConstantHelper.KEY_ID));
                                gestation.setBabyName(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_GESTATION_NAME));
                                gestation.setBabyGender(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_GESTATION_GENDER));
                                gestation.setDueDate(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_GESTATION_HPL));
                                gestation.setInsemination(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_GESTATION_INSEMINATION));
                                gestation.setFirstPregnant(responseArray.getJSONObject(i).getBoolean(ConstantHelper.KEY_GESTATION_FIRST_GESTATION));
                                gestation.setBeenBorn(responseArray.getJSONObject(i).getBoolean(ConstantHelper.KEY_GESTATION_BEEN_BORN));
                                gestation.setMissBirth(responseArray.getJSONObject(i).getBoolean(ConstantHelper.KEY_GESTATION_MIS_BIRTH));
                                gestationCard.setBabyName(gestation.getBabyName());
                                gestationCard.setBabyGender(gestation.getBabyGender());
                                gestationCard.setBabyHpl(gestation.getDueDate());

                                if (!gestation.isBeenBorn() && !gestation.isMissBirth()) {
                                    editBaby.setText("Ubah");
                                    gestationCard.setStatus("In progress");
                                    inseminationDate = responseArray.getJSONObject(i).getString(ConstantHelper.KEY_GESTATION_INSEMINATION);
                                    gestatinId = gestation.getId();
                                    babyName.setText(gestation.getBabyName());
                                    generateBabyGenderValue(gestation.getBabyGender());

                                    String date = DateHelper.convertToAndroid(gestation.getDueDate());
                                    babyHpl.setText(date);

                                    fistGestationSwitch.setChecked(gestation.isFirstPregnant());
                                    beenBornSwitch.setChecked(gestation.isBeenBorn());
                                    missBirthSwitch.setChecked(gestation.isMissBirth());
                                    disableElements(true);
                                } else {

                                    gestationCard.setStatus("Selesai");

                                    editBaby.setText("Tambah");
                                    babyName.setText("");
                                    generateBabyGenderValue("Tidak diketahui");
                                    calculateHpl.setText("Hitung hpl");
                                    fistGestationSwitch.setChecked(false);
                                    beenBornSwitch.setChecked(false);
                                    missBirthSwitch.setChecked(false);
                                    disableElements(false);

                                }

                                gestationList.add(gestation);

                                gestationCardModels.add(gestationCard);
                                gestationListAdapter = new GestationListAdapter(UserGestationFragment.this, gestationCardModels);
                            }
                            for (int x = 0; x < gestationListAdapter.getCount(); x++) {
                                View view = gestationListAdapter.getView(x, null, list_gestation);
                                list_gestation.addView(view);
                            }
                        }
                        loadPage(false);

                    } else {
                        String responseData = response.errorBody().string();
                        JSONObject json = new JSONObject(responseData);
                        String reason = json.getString("reason");
                        showMessage(root.getContext(), reason);
                        loadPage(false);

                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    loadPage(false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                loadPage(false);
                showMessage(root.getContext(), t.getMessage());
            }
        });
    }

    public void updateGestation(int id, Gestation gestation) throws JSONException {
        loadPage(true);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

        JSONObject paramObject = new JSONObject();
        paramObject.put(ConstantHelper.KEY_ID, gestation.getId());
        paramObject.put(ConstantHelper.KEY_GESTATION_NAME, gestation.getBabyName());
        paramObject.put(ConstantHelper.KEY_GESTATION_GENDER, gestation.getBabyGender());
        paramObject.put(ConstantHelper.KEY_GESTATION_INSEMINATION, gestation.getInsemination());
        paramObject.put(ConstantHelper.KEY_GESTATION_HPL, gestation.getDueDate());
        paramObject.put(ConstantHelper.KEY_GESTATION_FIRST_GESTATION, gestation.isFirstPregnant());
        paramObject.put(ConstantHelper.KEY_GESTATION_BEEN_BORN, gestation.isBeenBorn());
        paramObject.put(ConstantHelper.KEY_GESTATION_MIS_BIRTH, gestation.isMissBirth());

        Call<ResponseBody> call = apiInterface.updateGestationDetail(String.valueOf(id), paramObject.toString());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.code() != 200) {
                        String responseData = response.errorBody().string();
                        JSONObject json = new JSONObject(responseData);
                        String reason = json.getString("reason");
                        showMessage(root.getContext(), reason);

                    }
                    loadPage(false);
                    reloadActivity();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    loadPage(false);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                loadPage(false);
                showMessage(root.getContext(), t.getMessage());
            }
        });
    }

    public void addGestationDetails(String userId, Gestation gestation) throws JSONException {
        loadPage(true);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

        JSONObject paramObject = new JSONObject();
        paramObject.put(ConstantHelper.KEY_USERID, userId);
        paramObject.put(ConstantHelper.KEY_GESTATION_NAME, gestation.getBabyName());
        paramObject.put(ConstantHelper.KEY_GESTATION_GENDER, gestation.getBabyGender());
        paramObject.put(ConstantHelper.KEY_GESTATION_INSEMINATION, gestation.getInsemination());
        paramObject.put(ConstantHelper.KEY_GESTATION_HPL, gestation.getDueDate());
        paramObject.put(ConstantHelper.KEY_GESTATION_FIRST_GESTATION, gestation.isFirstPregnant());
        paramObject.put(ConstantHelper.KEY_GESTATION_BEEN_BORN, gestation.isBeenBorn());
        paramObject.put(ConstantHelper.KEY_GESTATION_MIS_BIRTH, gestation.isMissBirth());

        Call<ResponseBody> call = apiInterface.createGestationDetail(paramObject.toString());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    reloadActivity();

                    SessionManager.removeGestationSession();

                } else {
                    loadPage(false);
                    try {
                        String errorJson = response.errorBody().string();
                        JSONObject json = new JSONObject(errorJson);

                        showMessage(root.getContext(), json.getString("reason"));
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadPage(false);
                showMessage(root.getContext(), t.getMessage());
            }
        });
    }

    public void initElement() {

        loading_progress = root.findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);
        homeScroll = root.findViewById(R.id.homeScroll);
        loadingSection = root.findViewById(R.id.loadingSection);

        editBaby = root.findViewById(R.id.editBaby);
        babyName = root.findViewById(R.id.babyName);
        babyGender = root.findViewById(R.id.babyGender);
        babyHpl = root.findViewById(R.id.babyHpl);
        babyHpl.setEnabled(false);
        calculateHpl = root.findViewById(R.id.calculateHpl);
        fistGestationSwitch = root.findViewById(R.id.fistGestationSwitch);
        beenBornSwitch = root.findViewById(R.id.beenBornSwitch);
        missBirthSwitch = root.findViewById(R.id.missBirthSwitch);

        list_gestation = root.findViewById(R.id.list_gestation);

        disableElements(true);

    }

    public void disableElements(boolean status) {
        if (!status) {
            babyName.setEnabled(true);
            babyGender.setEnabled(true);
            calculateHpl.setEnabled(true);
            fistGestationSwitch.setEnabled(true);
            beenBornSwitch.setEnabled(true);
            missBirthSwitch.setEnabled(true);
        } else {
            babyName.setEnabled(false);
            babyGender.setEnabled(false);
            calculateHpl.setEnabled(false);
            fistGestationSwitch.setEnabled(false);
            beenBornSwitch.setEnabled(false);
            missBirthSwitch.setEnabled(false);
        }
    }

    private void loadPage(boolean states) {
        if (states) {
            loading_progress.setVisibility(View.VISIBLE);
            homeScroll.setEnableScrolling(false);
            loadingSection.setVisibility(View.VISIBLE);
        } else {
            loading_progress.setVisibility(View.GONE);
            homeScroll.setEnableScrolling(true);
            loadingSection.setVisibility(View.GONE);
        }
    }

    private void generateBabyGenderValue(String gender) {
        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.babyGenders);

        babyGender.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition(gender);
        babyGender.setSelection(spinnerPosition);
    }

    private void reloadActivity() {
        reloadActivity(UserGestationFragment.this);

        gestationCardModels.clear();
    }

}
