package id.io.mypregnancy.ui.fragment;

public class AccountFragmentArchived extends BaseFragment {
/*
    // Root elements
    View root;

    ProgressBar loading_progress;
    CustomScrollView homeScroll;
    RelativeLayout loadingSection;
    TextView editAccount, editAddress;
    EditText userEmail, userPhone, userAddress, userZone;
    Spinner userAge, userType;
    MapView userLocationMap;
    Button btnGetLocation, btnLogout;
    GoogleMap googleMap;
    UiSettings mUiSettings;
    Bundle mapsBundle;

    String longitude, latitude;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_account, container, false);

        initElements();
        loadPage(false, loading_progress, homeScroll, loadingSection);
        mapsBundle = savedInstanceState;

        sessionManager(root.getContext());

        generateAgeValue("0");
        generateRelationValue("lainnya");

        //initiateSessionToElements(SessionManager.getUserSession());

        if (SessionManager.isSavedLocationSession()) {
            JSONObject sessionLocation = SessionManager.getLocationSession();
            enableAddressSection(true);

            try {
                latitude = sessionLocation.getString(ConstantHelper.KEY_LATITUDE);
                longitude = sessionLocation.getString(ConstantHelper.KEY_LONGITUDE);

                generateMapsPosition(Float.valueOf(latitude), Float.valueOf(longitude));
                SessionManager.revokeLocationSession();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        editAccount.setOnClickListener(v -> {
            String state = editAccount.getText().toString();

            if (state.contains("Ubah")) {
                enableAccountSection(true);
            } else {

                //User user = SessionManager.getUserSession();
                //user.setPhone(userPhone.getText().toString());
               // user.setAge(userAge.getSelectedItem().toString());
                //user.setRelation(userType.getSelectedItem().toString());
                //updateUser(user);

                editAccount.setText("Ubah");
                editAccount.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_blue));

                disableElemets();
            }

        });

        editAddress.setOnClickListener(v -> {
            String state = editAddress.getText().toString();

            if (state.contains("Ubah")) {
                enableAddressSection(true);
            } else {

                //UserLocation userLocation = SessionManager.getUserLocationSession();

                    *//*locationJson.put(ConstantHelper.KEY_LOCATION_ADDRESS, userAddress.getText().toString());
                    locationJson.put(ConstantHelper.KEY_LOCATION_ZONE, userZone.getText().toString());
                    locationJson.put(ConstantHelper.KEY_LATITUDE, latitude);
                    locationJson.put(ConstantHelper.KEY_LONGITUDE, longitude);
                    userJson.put(ConstantHelper.KEY_USER_LOCATION, locationJson);
*//*
                //updateUser(userJson);

                editAddress.setText("Ubah");
                editAddress.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_blue));

                userLocationMap.setVisibility(View.VISIBLE);

                disableElemets();
            }

        });

        btnGetLocation.setOnClickListener(v -> {
            moveFragment(AccountFragmentArchived.this, new GetCurrentLocationFragment());
        });

        btnLogout.setOnClickListener(v -> SessionManager.logoutUserSession());

        return root;
    }

    public void initiateSessionToElements(JSONObject json) {

        try {
            cacheUserId = json.getString(ConstantHelper.KEY_USERID);

            userEmail.setText(json.getString(ConstantHelper.KEY_EMAIL));
            userPhone.setText(json.getString(ConstantHelper.KEY_PHONE));

            if (json.has(ConstantHelper.KEY_AGES)) {
                generateAgeValue(json.getString(ConstantHelper.KEY_AGES));
            }
            if (json.has(ConstantHelper.KEY_RELATION)) {
                generateRelationValue(json.getString(ConstantHelper.KEY_RELATION));
            }
            if (json.has(ConstantHelper.KEY_USER_LOCATION)) {
                JSONObject locationJson = json.getJSONObject(ConstantHelper.KEY_USER_LOCATION);
                if (locationJson.has(ConstantHelper.KEY_LOCATION_ADDRESS)) {
                    userAddress.setText(locationJson.getString(ConstantHelper.KEY_LOCATION_ADDRESS));
                }
                if (locationJson.has(ConstantHelper.KEY_LOCATION_ZONE)) {
                    userZone.setText(locationJson.getString(ConstantHelper.KEY_LOCATION_ZONE));
                }
                if (locationJson.has(ConstantHelper.KEY_LATITUDE)) {
                    latitude = locationJson.getString(ConstantHelper.KEY_LATITUDE);
                }
                if (locationJson.has(ConstantHelper.KEY_LONGITUDE)) {
                    longitude = locationJson.getString(ConstantHelper.KEY_LONGITUDE);
                }
            }
            if (latitude != null && longitude != null) {
                userLocationMap.setVisibility(View.VISIBLE);
                generateMapsPosition(Float.valueOf(latitude), Float.valueOf(longitude));
            }

        } catch (JSONException e) {
            showMessage(root.getContext(), e.getMessage());
        }

    }

    private void generateAgeValue(String age) {

        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.ages);
        userAge.setAdapter(adapter);
        int ages = Integer.parseInt(age);
        if (ages < 21) {
            int spinnerPosition = adapter.getPosition("Dibawah 21 tahun");
            userAge.setSelection(spinnerPosition);
        } else if (ages >= 21) {
            int spinnerPosition = adapter.getPosition(age);
            userAge.setSelection(spinnerPosition);
        } else if (ages > 40) {
            int spinnerPosition = adapter.getPosition("40+");
            userAge.setSelection(spinnerPosition);
        }
    }

    private void generateRelationValue(String relation) {
        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.relations);

        userType.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition(relation);
        userType.setSelection(spinnerPosition);

    }

    private void generateMapsPosition(Float latitude, Float longitude) {

        userLocationMap.onCreate(mapsBundle);
        userLocationMap.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        userLocationMap.getMapAsync(mMap -> {
            googleMap = mMap;
            mUiSettings = mMap.getUiSettings();

            mUiSettings.setZoomGesturesEnabled(false);
            mUiSettings.setTiltGesturesEnabled(false);
            mUiSettings.setMapToolbarEnabled(true);
            mUiSettings.setMyLocationButtonEnabled(false);
            mUiSettings.setAllGesturesEnabled(false);
            mUiSettings.setZoomControlsEnabled(false);


            // For showing a move to my location button
            if (ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);

            // For dropping a marker at a point on the Map
            LatLng currentLocation = new LatLng(latitude, longitude);
            googleMap.addMarker(new MarkerOptions().position(currentLocation).title("Your Location"));

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }

    private void updateUser(JSONObject json) throws JSONException {

        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

        JSONObject paramObject = new JSONObject();
        paramObject.put(ConstantHelper.KEY_USERID, json.getString(ConstantHelper.KEY_USERID));
        paramObject.put(ConstantHelper.KEY_GIVENNAME, json.getString(ConstantHelper.KEY_GIVENNAME));
        paramObject.put(ConstantHelper.KEY_FULLNAME, json.getString(ConstantHelper.KEY_FULLNAME));
        paramObject.put(ConstantHelper.KEY_EMAIL, json.getString(ConstantHelper.KEY_EMAIL));
        paramObject.put(ConstantHelper.KEY_PHONE, json.getString(ConstantHelper.KEY_PHONE));

        if (json.has(ConstantHelper.KEY_AGES)) {
            paramObject.put(ConstantHelper.KEY_AGES, json.getString(ConstantHelper.KEY_AGES));
        }
        if (json.has(ConstantHelper.KEY_RELATION)) {
            paramObject.put(ConstantHelper.KEY_RELATION, json.getString(ConstantHelper.KEY_RELATION));
        }

        JSONObject paramLocation = new JSONObject();

        if (json.has(ConstantHelper.KEY_USER_LOCATION)) {
            JSONObject locationObject = json.getJSONObject(ConstantHelper.KEY_USER_LOCATION);
            if (locationObject.has(ConstantHelper.KEY_LOCATION_ADDRESS)) {
                paramLocation.put(ConstantHelper.KEY_LOCATION_ADDRESS, locationObject.getString(ConstantHelper.KEY_LOCATION_ADDRESS));
            }
            if (locationObject.has(ConstantHelper.KEY_LOCATION_ZONE)) {
                paramLocation.put(ConstantHelper.KEY_LOCATION_ZONE, locationObject.getString(ConstantHelper.KEY_LOCATION_ZONE));
            }
            if (locationObject.has(ConstantHelper.KEY_LATITUDE)) {
                paramLocation.put(ConstantHelper.KEY_LATITUDE, locationObject.getString(ConstantHelper.KEY_LATITUDE));
            }
            if (locationObject.has(ConstantHelper.KEY_LONGITUDE)) {
                paramLocation.put(ConstantHelper.KEY_LONGITUDE, locationObject.getString(ConstantHelper.KEY_LONGITUDE));
            }

            paramObject.put(ConstantHelper.KEY_USER_LOCATION, paramLocation);
        }

        Call<ResponseBody> call = apiInterface.updateUser(paramObject.toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
*//*                    try {
                        SessionManager.userSessionUpdate();
                        //SessionManager.setUserSession(json);
                        showMessage(root.getContext(), MessageConstant.MESSAGE_DETAILS_HAS_CHANGED);
                        reloadActivity(AccountFragment.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*//*
                } else {
                    showMessage(root.getContext(), ErrorReasonConstant.REASON_FAILED_USER_UPDATE);
                    loadPage(false, loading_progress, homeScroll, loadingSection);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                showMessage(root.getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }
        });
    }

    private void initElements() {

        loading_progress = root.findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);
        homeScroll = root.findViewById(R.id.homeScroll);
        loadingSection = root.findViewById(R.id.loadingSection);

        //editAccount = root.findViewById(R.id.editAccount);
        userEmail = root.findViewById(R.id.userEmail);
        userPhone = root.findViewById(R.id.userPhone);

        userAge = root.findViewById(R.id.userAge);
        userType = root.findViewById(R.id.userType);

        //editAddress = root.findViewById(R.id.editAddress);
        userAddress = root.findViewById(R.id.userAddress);
        userZone = root.findViewById(R.id.userZone);
        userLocationMap = root.findViewById(R.id.userLocationMap);
        btnGetLocation = root.findViewById(R.id.btnGetLocation);

        btnLogout = root.findViewById(R.id.btnLogout);
        userLocationMap.setVisibility(View.GONE);

        disableElemets();

    }

    private void enableAccountSection(boolean enable) {
        if (enable) {
            userPhone.setEnabled(true);
            userAge.setEnabled(true);
            userType.setEnabled(true);
            editAccount.setText("Simpan");
            editAccount.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_green));
        }

    }

    private void enableAddressSection(boolean enable) {
        if (enable) {
            userAddress.setEnabled(true);
            userZone.setEnabled(true);
            btnGetLocation.setEnabled(true);
            editAddress.setText("Simpan");
            editAddress.setBackground(ContextCompat.getDrawable(root.getContext(), R.drawable.style_button_green));
            userLocationMap.setVisibility(View.VISIBLE);
        }
    }

    private void disableElemets() {

        userEmail.setEnabled(false);
        userPhone.setEnabled(false);
        userAge.setEnabled(false);
        userType.setEnabled(false);

        userAddress.setEnabled(false);
        userZone.setEnabled(false);
        userLocationMap.setEnabled(false);
        btnGetLocation.setEnabled(false);

        btnGetLocation.setEnabled(false);

    }*/
}
