package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import id.io.mypregnancy.R;
import id.io.mypregnancy.adapter.ScheduleListAdapter;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.helper.DateHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.ScheduleCard;
import id.io.mypregnancy.ui.custom.CustomScrollView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSchedulesFragment extends BaseFragment {
    //root elements
    View root;

    TextView addSchedule;
    ProgressBar loading_progress;
    RelativeLayout loading_pages;
    CustomScrollView homeScroll;

    ListView scheduleList;

    //helper
    String userid;
    ScheduleListAdapter scheduleListAdapter;
    List<ScheduleCard> scheduleCardList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_user_schedules, container, false);

        initElement();

        sessionManager(root.getContext());

        userid = SessionManager.getLoginSession().get(ConstantHelper.KEY_USERID);
        getScheduleList(userid);

        addSchedule.setOnClickListener(v -> {
            moveFragment(UserSchedulesFragment.this, new UserScheduleAddFragment());
        });

        scheduleList.setOnItemClickListener((parent, view, position, id) -> {
            String scheduleId = String.valueOf(scheduleCardList.get(position).getId());
            showMessage(root.getContext(), scheduleId);
        });

        return root;
    }

    public void getScheduleList(String userId) {
        loadPage(true, loading_progress, homeScroll, loading_pages);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.getScheduleList(userId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.code() == 200) {

                        JSONArray responseArray = new JSONArray(response.body().string());

                        for (int i = 0; i < responseArray.length(); i++) {

                            ScheduleCard scheduleCard = new ScheduleCard();

                            scheduleCard.setId(responseArray.getJSONObject(i).getInt(ConstantHelper.KEY_SCHEDULE_ID));
                            scheduleCard.setScheduleName(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_SCHEDULE_NAME));
                            scheduleCard.setAppointmentTime(DateHelper.convertTimeToAndroid(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_SCHEDULE_TIME)));
                            scheduleCard.setAppointmentDate(DateHelper.convertToAndroid(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_SCHEDULE_DATE)));
                            scheduleCard.setStatus(responseArray.getJSONObject(i).getString(ConstantHelper.KEY_STATUS));

                            scheduleCardList.add(scheduleCard);
                        }

                        scheduleListAdapter = new ScheduleListAdapter(UserSchedulesFragment.this, scheduleCardList);
                        scheduleList.setAdapter(scheduleListAdapter);

                    } else {
                        String responseData = response.errorBody().string();
                        JSONObject json = new JSONObject(responseData);
                        String reason = json.getString("reason");
                        showMessage(root.getContext(), reason);

                    }
                    loadPage(false, loading_progress, homeScroll, loading_pages);
                } catch (IOException | JSONException e) {
                    showMessage(root.getContext(), e.getMessage());
                    loadPage(false, loading_progress, homeScroll, loading_pages);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                showMessage(root.getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loading_pages);
            }
        });
    }


    private void initElement() {
        addSchedule = root.findViewById(R.id.addSchedule);

        scheduleList = root.findViewById(R.id.scheduleList);

        loading_progress = root.findViewById(R.id.loading_progress);
        loading_pages = root.findViewById(R.id.loading_pages);
        homeScroll = root.findViewById(R.id.homeScroll);
    }

}
