package id.io.mypregnancy.ui.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.UserAddress;
import id.io.mypregnancy.ui.custom.CustomScrollView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountUserAddressFragment extends BaseFragment {

    // Root elements
    View root;

    Spinner provinceSpinner, regencySpinner, districtSpinner, villageSpinner, postalCodeSpinner;
    EditText txtAddress, txtProvince, txtRegency, txtDistrict, txtVillage, txtPostalCode;

    MapView userLocationMap;
    Button btnGetLocation, btnAction;
    GoogleMap googleMap;
    UiSettings mUiSettings;
    Bundle mapsBundle;

    String longitude, latitude;

    ProgressBar loading_progress;
    CustomScrollView homeScroll;
    RelativeLayout loadingSection;

    List<String> provinceList = new ArrayList<String>();
    List<String> regencyList = new ArrayList<String>();
    List<String> districtList = new ArrayList<String>();
    List<String> villageList = new ArrayList<String>();
    List<String> postalCodeList = new ArrayList<String>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_account_useraddress, container, false);
        init();

        regencyList.add("<Pilih>");
        districtList.add("<Pilih>");
        villageList.add("<Pilih>");
        postalCodeList.add("<Pilih>");

        mapsBundle = savedInstanceState;

        sessionManager(root.getContext());

        try {
            loadPage(true, loading_progress, homeScroll, loadingSection);
            if (!SessionManager.hasUserAddressSession()) {
                renewSession();
                showMessage(root.getContext(), "NEW DATA");
                getUserAddress(SessionManager.getUserIdSession());
            } else if (SessionManager.hasUserAddressCacheSession()) {
                showMessage(root.getContext(), "UPDATE CACHE DATA");
                UserAddress addressCache = SessionManager.getUserAddressCacheSession();
                mapContent(addressCache);
            } else {
                renewSession();
                showMessage(root.getContext(), "SESSION DATA");
                UserAddress addressSession = SessionManager.getUserAddressSession();
                mapContent(addressSession);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        txtProvince.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                provinceSpinner.performClick();
                getProvinceList();
                return false;
            }
        });
/*        txtProvince.setOnTouchListener(v -> {
            if (btnAction.getText().equals("Simpan")) {
                txtProvince.setText("");
                txtProvince.setVisibility(View.INVISIBLE);
                //provinceSpinner.performClick();
                //getProvinceList();

            } else if (btnAction.getText().equals("Ubah")) {
                txtProvince.setVisibility(View.VISIBLE);
                provinceSpinner.setVisibility(View.INVISIBLE);
            }

        });*/

        btnGetLocation.setOnClickListener(v -> {
            UserAddress addressCache = new UserAddress();
            addressCache.setProvince(provinceSpinner.getSelectedItem().toString());
            addressCache.setDistrict(districtSpinner.getSelectedItem().toString());
            addressCache.setRegency(regencySpinner.getSelectedItem().toString());
            addressCache.setVillage(villageSpinner.getSelectedItem().toString());
            addressCache.setPostalCode(postalCodeSpinner.getSelectedItem().toString());
            addressCache.setAddress(txtAddress.getText().toString());

            SessionManager.setUserAddressCacheSession(addressCache);


            moveFragment(AccountUserAddressFragment.this, new GetCurrentLocationFragment());
        });

        btnAction.setOnClickListener(v -> {
            if (btnAction.getText().equals("Simpan")) {
                SessionManager.removeUserAddressCacheSession();
                updateStatus(false);
                //actionStatus(false);
            } else if (btnAction.getText().equals("Ubah")) {
                updateStatus(true);
                //actionStatus(true);
            }
        });

        provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!provinceSpinner.getSelectedItem().equals("<Pilih>")) {
                } else {
                    txtProvince.setText(provinceSpinner.getSelectedItem().toString());
                    provinceSpinner.setVisibility(View.INVISIBLE);
                    txtProvince.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        regencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!regencySpinner.getSelectedItem().equals("<Pilih>")) {
                    districtList.clear();
                    villageList.clear();
                    postalCodeList.clear();

                    districtList.add("<Pilih>");
                    villageList.add("<Pilih>");
                    postalCodeList.add("<Pilih>");

                    getDistrictList(regencySpinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!districtSpinner.getSelectedItem().equals("<Pilih>")) {
                    villageList.clear();
                    postalCodeList.clear();
                    villageList.add("<Pilih>");
                    postalCodeList.add("<Pilih>");
                    getVillageList(districtSpinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        villageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!villageSpinner.getSelectedItem().equals("<Pilih>")) {
                    postalCodeList.clear();
                    postalCodeList.add("<Pilih>");
                    getPostalCodeList(districtSpinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;

    }

    private void init() {
        provinceSpinner = root.findViewById(R.id.provinceSpinner);
        regencySpinner = root.findViewById(R.id.regencySpinner);
        villageSpinner = root.findViewById(R.id.villageSpinner);
        districtSpinner = root.findViewById(R.id.districtSpinner);
        postalCodeSpinner = root.findViewById(R.id.postalCodeSpinner);
        txtAddress = root.findViewById(R.id.userAddress);
        userLocationMap = root.findViewById(R.id.userLocationMap);
        btnGetLocation = root.findViewById(R.id.btnGetLocation);
        btnAction = root.findViewById(R.id.btnAction);

        txtProvince = root.findViewById(R.id.txtProvince);
        txtRegency = root.findViewById(R.id.txtRegency);
        txtDistrict = root.findViewById(R.id.txtDistrict);
        txtVillage = root.findViewById(R.id.txtVillage);
        txtPostalCode = root.findViewById(R.id.txtPostalCode);

        if (btnAction.getText().equals("Ubah")) {
            updateStatus(false);
        }

        provinceSpinner.setVisibility(View.INVISIBLE);
        regencySpinner.setVisibility(View.INVISIBLE);
        villageSpinner.setVisibility(View.INVISIBLE);
        districtSpinner.setVisibility(View.INVISIBLE);
        postalCodeSpinner.setVisibility(View.INVISIBLE);

        loading_progress = root.findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);
        homeScroll = root.findViewById(R.id.homeScroll);
        loadingSection = root.findViewById(R.id.loadingSection);
    }

    private void generateProvinceSpinner(String province) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_spinner_item, provinceList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        provinceSpinner.setAdapter(spinnerArrayAdapter);

        int spinnerPosition = spinnerArrayAdapter.getPosition(province);
        provinceSpinner.setSelection(spinnerPosition);

    }

    private void generateRegencySpinner(String regency) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_spinner_item, regencyList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        regencySpinner.setAdapter(spinnerArrayAdapter);

        int spinnerPosition = spinnerArrayAdapter.getPosition(regency);
        regencySpinner.setSelection(spinnerPosition);
    }

    private void generateDistrictSpinner(String district) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_spinner_item, districtList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        districtSpinner.setAdapter(spinnerArrayAdapter);

        int spinnerPosition = spinnerArrayAdapter.getPosition(district);
        districtSpinner.setSelection(spinnerPosition);
    }

    private void generateVillageSpinner(String village) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_spinner_item, villageList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        villageSpinner.setAdapter(spinnerArrayAdapter);

        int spinnerPosition = spinnerArrayAdapter.getPosition(village);
        villageSpinner.setSelection(spinnerPosition);
    }

    private void generatePostalCodeSpinner(String postalCode) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_spinner_item, postalCodeList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        postalCodeSpinner.setAdapter(spinnerArrayAdapter);

        int spinnerPosition = spinnerArrayAdapter.getPosition(postalCode);
        postalCodeSpinner.setSelection(spinnerPosition);
    }

    private void getProvinceList() {
        provinceList.clear();
        provinceList.add("<Pilih>");
        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.getProvinceList();
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String json = response.body().string();
                        List<String> jsonResult = new ArrayList<String>();
                        jsonResult = JsonHelper.fromJson(json, List.class);
                        for (String value : jsonResult) {
                            provinceList.add(value);
                        }
                        generateProvinceSpinner("<PILIH>");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }
        });
    }

    private void getRegencyList(String province) {
        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.getRegencyList(province);
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String json = response.body().string();
                        List<String> jsonResult = new ArrayList<String>();
                        jsonResult = JsonHelper.fromJson(json, List.class);
                        for (String value : jsonResult) {
                            regencyList.add(value);
                        }
                        generateRegencySpinner("<PILIH>");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }
        });
    }

    private void getDistrictList(String regency) {
        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.getDistrictList(regency);
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String json = response.body().string();
                        List<String> jsonResult = new ArrayList<String>();
                        jsonResult = JsonHelper.fromJson(json, List.class);
                        for (String value : jsonResult) {
                            districtList.add(value);
                        }
                        generateDistrictSpinner("<PILIH>");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }
        });
    }

    private void getVillageList(String district) {
        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.getVillageList(district);
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String json = response.body().string();
                        List<String> jsonResult = new ArrayList<String>();
                        jsonResult = JsonHelper.fromJson(json, List.class);
                        for (String value : jsonResult) {
                            villageList.add(value);
                        }
                        generateVillageSpinner("<PILIH>");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }
        });
    }

    private void getPostalCodeList(String district) {
        loadPage(true, loading_progress, homeScroll, loadingSection);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.getPostalCodeList(district);
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String json = response.body().string();
                        List<String> jsonResult = new ArrayList<String>();
                        jsonResult = JsonHelper.fromJson(json, List.class);
                        for (String value : jsonResult) {
                            postalCodeList.add(value);
                        }
                        generatePostalCodeSpinner("<PILIH>");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getContext(), t.getMessage());
                loadPage(false, loading_progress, homeScroll, loadingSection);
            }
        });
    }

    private void mapContent(UserAddress content) {

        txtProvince.setText(content.getProvince());
        txtRegency.setText(content.getRegency());
        txtDistrict.setText(content.getRegency());
        txtVillage.setText(content.getVillage());
        txtPostalCode.setText(content.getPostalCode());
        txtAddress.setText(content.getAddress());
        latitude = String.valueOf(content.getLatitude());
        longitude = String.valueOf(content.getLongitude());
        userLocationMap.setVisibility(View.VISIBLE);

        generateMapsPosition(Float.valueOf(latitude), Float.valueOf(longitude));

        loadPage(false, loading_progress, homeScroll, loadingSection);

    }

    private void generateMapsPosition(Float latitude, Float longitude) {

        userLocationMap.onCreate(mapsBundle);
        userLocationMap.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        userLocationMap.getMapAsync(mMap -> {
            googleMap = mMap;
            mUiSettings = mMap.getUiSettings();

            mUiSettings.setZoomGesturesEnabled(false);
            mUiSettings.setTiltGesturesEnabled(false);
            mUiSettings.setMapToolbarEnabled(true);
            mUiSettings.setMyLocationButtonEnabled(false);
            mUiSettings.setAllGesturesEnabled(false);
            mUiSettings.setZoomControlsEnabled(false);


            // For showing a move to my location button
            if (ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);

            // For dropping a marker at a point on the Map
            LatLng currentLocation = new LatLng(latitude, longitude);
            googleMap.addMarker(new MarkerOptions().position(currentLocation).title("Your Location"));

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }

    public void getUserAddress(String userId) {

        loadPage(true, loading_progress, homeScroll, loadingSection);

        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> getUserAddress = apiInterface.getUserAddress(userId);

        getUserAddress.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> userCall, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        String json = response.body().string();
                        UserAddress address = JsonHelper.fromJson(json, UserAddress.class);
                        SessionManager.setUserAddressSession(address);
                        mapContent(address);

                        loadPage(false, loading_progress, homeScroll, loadingSection);
                    }
                } catch (IOException e) {
                    loadPage(false, loading_progress, homeScroll, loadingSection);
                    showMessage(root.getContext(), e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                loadPage(false, loading_progress, homeScroll, loadingSection);
                showMessage(root.getContext(), t.getMessage());
            }
        });

    }

    private void disableElement(boolean enable) {
        if (enable) {
            txtProvince.setEnabled(false);
            txtRegency.setEnabled(false);
            txtDistrict.setEnabled(false);
            txtVillage.setEnabled(false);
            txtPostalCode.setEnabled(false);
            txtAddress.setEnabled(false);
            userLocationMap.setEnabled(false);
            btnGetLocation.setEnabled(false);

        } else {
            txtProvince.setEnabled(true);
            txtRegency.setEnabled(true);
            txtDistrict.setEnabled(true);
            txtVillage.setEnabled(true);
            txtPostalCode.setEnabled(true);
            txtAddress.setEnabled(true);
            userLocationMap.setEnabled(true);
            btnGetLocation.setEnabled(true);
        }
    }

    private void updateStatus(boolean status) {
        if (status) {
            disableElement(false);
            btnAction.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.style_button_green));
            btnAction.setText("Simpan");
        } else {
            btnAction.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.style_button_blue));
            disableElement(true);
            btnAction.setText("Ubah");
        }
    }

    /*public void isUpdate(boolean status) {
        if (status) {

            txtProvince.setVisibility(View.INVISIBLE);
            txtRegency.setVisibility(View.INVISIBLE);
            txtDistrict.setVisibility(View.INVISIBLE);
            txtVillage.setVisibility(View.INVISIBLE);
            txtPostalCode.setVisibility(View.INVISIBLE);

            provinceSpinner.setVisibility(View.VISIBLE);
            regencySpinner.setVisibility(View.VISIBLE);
            villageSpinner.setVisibility(View.VISIBLE);
            districtSpinner.setVisibility(View.VISIBLE);
            postalCodeSpinner.setVisibility(View.VISIBLE);

            getProvinceList();
            generateRegencySpinner("<Pilih>");
            generateDistrictSpinner("<Pilih>");
            generateVillageSpinner("<Pilih>");
            generatePostalCodeSpinner("<Pilih>");

        } else {
            txtProvince.setVisibility(View.VISIBLE);
            txtRegency.setVisibility(View.VISIBLE);
            txtDistrict.setVisibility(View.VISIBLE);
            txtVillage.setVisibility(View.VISIBLE);
            txtPostalCode.setVisibility(View.VISIBLE);

            provinceSpinner.setVisibility(View.INVISIBLE);
            regencySpinner.setVisibility(View.INVISIBLE);
            villageSpinner.setVisibility(View.INVISIBLE);
            districtSpinner.setVisibility(View.INVISIBLE);
            postalCodeSpinner.setVisibility(View.INVISIBLE);
        }
    }*/
}
