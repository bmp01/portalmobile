package id.io.mypregnancy.ui.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.Doctor;
import id.io.mypregnancy.model.Hospital;
import id.io.mypregnancy.ui.custom.CustomScrollView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserScheduleAddFragment extends BaseFragment {
    //root elements
    View root;
    Button btnSave;
    EditText scheduleName;
    Spinner scheduleCity;
    Spinner scheduleHospital;
    Spinner scheduleDoctor;
    EditText scheduleDate;
    EditText scheduleTime;
    EditText scheduleMotherWeight;
    EditText scheduleBloodPressure;
    EditText scheduleGestationHeartRate;
    EditText scheduleNotes;

    ProgressBar loading_progress;
    RelativeLayout loading_pages;
    CustomScrollView homeScroll;

    String userid;
    String hospitalId;
    String doctorId;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_user_schedule_add, container, false);

        initElement();
        sessionManager(root.getContext());
        userid = SessionManager.getLoginSession().get(ConstantHelper.KEY_USERID);

        scheduleDate.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                Calendar calendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DatePickerDialogTheme, (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    String date = simpleDateFormat.format(newDate.getTime());
                    scheduleDate.setText(date);
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });

        scheduleTime.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(root.getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hours = "";
                        String minutes = "";

                        if (selectedHour == 0) {
                            hours = "00";
                        } else if (selectedHour < 10) {
                            hours = "0" + selectedHour;
                        } else if (selectedHour > 10) {
                            hours = String.valueOf(selectedHour);
                        }

                        if (selectedMinute == 0) {
                            minutes = "00";

                        } else if (selectedMinute < 10) {
                            minutes = "0" + selectedMinute;
                        } else if (selectedMinute > 10) {
                            minutes = String.valueOf(selectedMinute);
                        }

                        scheduleTime.setText(hours + ":" + minutes);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        scheduleCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                scheduleCity.getSelectedItem();

                if (!scheduleCity.getSelectedItem().toString().equals("pilih kota")) {

                    getHospitalList(scheduleCity.getSelectedItem().toString());

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        scheduleHospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!scheduleHospital.getSelectedItem().toString().equals("pilih rumah sakit") && !scheduleHospital.getSelectedItem().toString().equals("")) {
                    Hospital hospital = (Hospital) scheduleHospital.getItemAtPosition(position);
                    getDoctorByHospital(String.valueOf(hospital.getId()));
                    hospitalId = String.valueOf(hospital.getId());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        scheduleDoctor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!scheduleDoctor.getSelectedItem().toString().equals("pilih dokter") && !scheduleDoctor.getSelectedItem().toString().equals("") && !scheduleDoctor.getSelectedItem().toString().isEmpty()) {
                    Doctor doctor = (Doctor) scheduleDoctor.getItemAtPosition(position);
                    doctorId = doctor.getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnSave.setOnClickListener(v -> {


            try {
                JSONObject request = new JSONObject();
                request.put("schedule-name", scheduleName.getText());
                request.put("user-id", userid);
                request.put("hospital-id", hospitalId);
                request.put("doctor-id", doctorId);
                request.put("appointment-date", scheduleDate.getText());
                request.put("appointment-time", scheduleTime.getText() + ":00");
                request.put("mother-weight", scheduleMotherWeight.getText());
                request.put("blood-pressure", scheduleBloodPressure.getText());
                request.put("heart-rate", scheduleGestationHeartRate.getText());
                request.put("notes", scheduleNotes.getText());

                saveUserSchedule(request);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        });
        return root;
    }


    private void generateCityValues(String position) {
        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.city);
        scheduleCity.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition(position);
        scheduleCity.setSelection(spinnerPosition);

    }

    private void generateHospitalValues(String position) {

        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.hospital);

        scheduleHospital.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition(position);
        scheduleHospital.setSelection(spinnerPosition);

    }

    private void generateDoctorValues(String position) {
        ArrayAdapter<CharSequence> adapter = generateDropdown(root.getContext(), R.array.doctor);

        scheduleDoctor.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition(position);
        scheduleDoctor.setSelection(spinnerPosition);

    }

    private void getHospitalList(String city) {
        loadPage(true, loading_progress, homeScroll, loading_pages);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

        Call<ResponseBody> call = apiInterface.getHospitalList(city);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONArray responseArray = new JSONArray(response.body().string());
                    List<Hospital> hospitalList = new ArrayList<>();
                    for (int i = 0; i < responseArray.length(); i++) {
                        Hospital hospital = new Hospital();
                        hospital.setId(responseArray.getJSONObject(i).getInt("id"));
                        hospital.setName(responseArray.getJSONObject(i).getString("name"));
                        hospitalList.add(hospital);
                    }
                    ArrayAdapter<Hospital> dataAdapter = new ArrayAdapter<Hospital>(root.getContext(), android.R.layout.simple_spinner_item, hospitalList);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    scheduleHospital.setAdapter(dataAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showMessage(root.getContext(), t.getMessage());
            }
        });
        loadPage(false, loading_progress, homeScroll, loading_pages);
    }

    private void getDoctorByHospital(String hospitalId) {

        if (!hospitalId.isEmpty() || !hospitalId.equals("pilih dokter") || !hospitalId.equals("")) {
            loadPage(true, loading_progress, homeScroll, loading_pages);
            APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

            Call<ResponseBody> call = apiInterface.getDoctor(hospitalId);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        JSONArray responseArray = new JSONArray(response.body().string());
                        List<Doctor> doctorList = new ArrayList<>();
                        for (int i = 0; i < responseArray.length(); i++) {
                            Doctor doctor = new Doctor();
                            doctor.setId(responseArray.getJSONObject(i).getString("id"));
                            doctor.setName(responseArray.getJSONObject(i).getString("name"));
                            doctorList.add(doctor);
                        }
                        ArrayAdapter<Doctor> dataAdapter = new ArrayAdapter<Doctor>(root.getContext(), android.R.layout.simple_spinner_item, doctorList);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        scheduleDoctor.setAdapter(dataAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
            loadPage(false, loading_progress, homeScroll, loading_pages);
        }
    }

    private void saveUserSchedule(JSONObject request) {
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

        Call<ResponseBody> call = apiInterface.addSchedule(userid, request.toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    showMessage(root.getContext(), "rencana periksa tersimpan");
                    moveFragment(UserScheduleAddFragment.this, new UserSchedulesFragment());
                } else {
                    try {
                        showMessage(root.getContext(), response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showMessage(root.getContext(), t.getMessage());
            }
        });


    }

    private void initElement() {

        btnSave = root.findViewById(R.id.btnSave);
        scheduleName = root.findViewById(R.id.scheduleName);
        scheduleCity = root.findViewById(R.id.scheduleCity);
        scheduleHospital = root.findViewById(R.id.scheduleHospital);
        scheduleDoctor = root.findViewById(R.id.scheduleDoctor);
        scheduleDate = root.findViewById(R.id.scheduleDate);
        scheduleTime = root.findViewById(R.id.scheduleTime);
        scheduleMotherWeight = root.findViewById(R.id.scheduleMotherWeight);
        scheduleBloodPressure = root.findViewById(R.id.scheduleBloodPressure);
        scheduleGestationHeartRate = root.findViewById(R.id.scheduleGestationHeartRate);
        scheduleNotes = root.findViewById(R.id.scheduleNotes);

        loading_progress = root.findViewById(R.id.loading_progress);
        loading_pages = root.findViewById(R.id.loading_pages);
        homeScroll = root.findViewById(R.id.homeScroll);
        loadPage(false, loading_progress, homeScroll, loading_pages);

        generateCityValues("pilih kota");
        generateHospitalValues("pilih rumah sakit");
        generateDoctorValues("pilih dokter");
    }
}
