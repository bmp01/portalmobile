package id.io.mypregnancy.ui.validator;

public class BaseValidator {

    public boolean notNull(Object obj) {
        return null != obj;
    }

    public boolean notNull(String str) {
        boolean result = false;
        if (!str.equalsIgnoreCase("")) {
            result = true;
        }
        return result;
    }
}
