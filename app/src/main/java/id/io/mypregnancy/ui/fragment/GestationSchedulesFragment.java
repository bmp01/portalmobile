package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;

import id.io.mypregnancy.R;

public class GestationSchedulesFragment extends BaseFragment {

    private View root;

    Button reloadFragment;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_gestation_schedules, container, false);

        initElement();

        reloadFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reloadActivity(GestationSchedulesFragment.this);
                showMessage(root.getContext(), "Activity reloaded");
            }
        });

        return root;
    }

    private void initElement() {
        reloadFragment = root.findViewById(R.id.btnReload);
    }
}
