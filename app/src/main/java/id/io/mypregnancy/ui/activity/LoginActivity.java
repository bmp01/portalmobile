package id.io.mypregnancy.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.api.model.AuthenticationRequest;
import id.io.mypregnancy.api.model.MobileResponse;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.constant.MessageConstant;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.ui.validator.MobileValidator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private MobileValidator validator;

    EditText email, password;
    Button btnLogin;
    ProgressBar login_progress;
    TextView directRegister;

    public LoginActivity() {
        // controller
        validator = new MobileValidator();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        init();

        btnLogin.setOnClickListener(v -> {
            AuthenticationRequest request = new AuthenticationRequest(email.getText().toString(), password.getText().toString());
            if (validator.validateEmail(request.getEmail())) {
                if (validator.validate(request)) {
                    authenticate(request);
                } else {
                    if (!validator.validate(request.getEmail()) && !validator.validate(request.getPassword())) {
                        showMessage(LoginActivity.this, MessageConstant.REASON_EMPTY_EMAIL_AND_PASSWORD_FIELD);
                    } else if (!validator.validate(request.getEmail())) {
                        showMessage(LoginActivity.this, MessageConstant.REASON_EMPTY_EMAIL_FIELD);
                    } else if (!validator.validate(request.getPassword())) {
                        showMessage(LoginActivity.this, MessageConstant.REASON_EMPTY_PASSWORD_FIELD);
                    }
                }
            } else {
                showMessage(LoginActivity.this, MessageConstant.EMAIL_NOT_VALID);
            }
        });
        directRegister.setOnClickListener(v -> movePage(LoginActivity.this, RegisterActivity.class));
    }

    public void authenticate(AuthenticationRequest request) {
        disableElement(true);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.login(JsonHelper.toJson(request));
        call.enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    switch (response.code()) {
                        case 200:

                            String responseData = response.body().string();
                            MobileResponse result = JsonHelper.fromJson(responseData, MobileResponse.class);
                            Map<String, String> userMap = new HashMap<>();
                            userMap.put(ConstantHelper.KEY_USERID, result.getUserId());
                            userMap.put(ConstantHelper.KEY_EMAIL, result.getEmail());

                            sessionManager(getApplicationContext());
                            if (result.isActive()) {
                                SessionManager.setLoginSession(userMap);
                                SessionManager.setUserIdSession(result.getUserId());
                                movePage(LoginActivity.this, MainActivity.class);
                            } else {
                                showMessage(LoginActivity.this, MessageConstant.REASON_ON_OTP_VALIDATION);
                                SessionManager.setRegisterSession(userMap);
                                movePage(LoginActivity.this, OtpActivity.class);
                            }
                            break;

                        case 400:
                            showMessage(LoginActivity.this, MessageConstant.REASON_BAD_REQUEST);
                            break;
                        case 401:
                            showMessage(LoginActivity.this, MessageConstant.REASON_INVALID_USERNAME_AND_OR_PASSWORD);
                            break;
                        case 404:
                            showMessage(LoginActivity.this, MessageConstant.REASON_EMAIL_NOT_FOUND);
                            break;
                        default:

                    }

                    disableElement(false);

                } catch (IOException e) {
                    showMessage(LoginActivity.this, e.getMessage());
                }
            }

            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showMessage(LoginActivity.this, t.getMessage());
                disableElement(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        movePage(LoginActivity.this, GetStartedActivity.class);
    }

    private void disableElement(boolean status) {
        if (!status) {
            email.setEnabled(true);
            password.setEnabled(true);
            login_progress.setVisibility(View.GONE);
            btnLogin.setEnabled(true);
        } else {
            email.setEnabled(false);
            password.setEnabled(false);
            btnLogin.setEnabled(false);
            login_progress.setVisibility(View.VISIBLE);
        }
    }

    public void init() {
        email = (EditText) findViewById(R.id.activityLoginEmail);
        password = (EditText) findViewById(R.id.activityLoginPassword);
        btnLogin = (Button) findViewById(R.id.activityLoginBtn);
        directRegister = (TextView) findViewById(R.id.directRegister);
        login_progress = (ProgressBar) findViewById(R.id.login_progress);
        login_progress.setVisibility(View.GONE);
    }
}