package id.io.mypregnancy.ui.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import org.json.JSONException;
import org.json.JSONObject;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.constant.ConstantHelper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class UserFragment extends BaseFragment implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback {

    View root;
    // User fragment elements
    LinearLayout userGestationSection;
    LinearLayout userScheduleSection;
    LinearLayout userDailyNotesSection;
    LinearLayout userTaskSection;
    Button emergencyButton;


    LocationManager locationManager;
    LocationListener locationListener;
    Location location;

    String lat;
    String lng;

    private int LOCATION_CODE = 1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_user, container, false);

        initElements();
        sessionManager(root.getContext());

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        userGestationSection.setOnClickListener(v -> {
            moveFragment(UserFragment.this, new UserGestationFragment());
        });
        userScheduleSection.setOnClickListener(v -> {
            moveFragment(UserFragment.this, new UserSchedulesFragment());
        });
/*        userDailyNotesSection.setOnClickListener(v -> {
            moveFragment(UserFragment.this, new UserDailyNotesFragment());
        });
        userTaskSection.setOnClickListener(v -> {
            moveFragment(UserFragment.this, new UserTaskFragment());
        });*/

        emergencyButton.setOnClickListener(V -> {

            getLocation();
            try {
                sendEmergency(lat, lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    root.getContext());

            alertDialogBuilder.setTitle("Bantuan Darurat");

            alertDialogBuilder
                    .setMessage("Petugas kami akan menghubungi bunda untuk konfirmasi dan lokasi terperinci, mohon tunggu beberapa saat")
                    .setIcon(R.mipmap.ic_launcher)
                    .setCancelable(false)
                    .setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {


                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        });

        return root;
    }

    private void sendEmergency(String latitude, String longitude) throws JSONException {

        JSONObject json = new JSONObject(); //SessionManager.getUserSession();
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);

        JSONObject paramObject = new JSONObject();
        paramObject.put(ConstantHelper.KEY_USERID, json.getString(ConstantHelper.KEY_USERID));
        paramObject.put(ConstantHelper.KEY_LATITUDE, latitude);
        paramObject.put(ConstantHelper.KEY_LONGITUDE, longitude);
        Call<ResponseBody> call = apiInterface.addEmergencyStatus(paramObject.toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void getLocation() {
        if (ContextCompat.checkSelfPermission(root.getContext(), ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            lat = String.valueOf(location.getLatitude());
            lng = String.valueOf(location.getLongitude());
        }
    }

    private void initElements() {
        userGestationSection = root.findViewById(R.id.userGestationSection);
        userScheduleSection = root.findViewById(R.id.userScheduleSection);
        //userDailyNotesSection = root.findViewById(R.id.userDailyNotesSection);
        //userTaskSection = root.findViewById(R.id.userTaskSection);
        emergencyButton = root.findViewById(R.id.emergencyButton);

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
