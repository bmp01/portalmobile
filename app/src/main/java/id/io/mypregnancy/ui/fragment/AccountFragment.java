package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import id.io.mypregnancy.R;
import id.io.mypregnancy.manager.SessionManager;

public class AccountFragment extends BaseFragment {

    // Root elements
    View root;

    LinearLayout accountSection, addressSection, logoutSection;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_account, container, false);

        initElements();

        sessionManager(root.getContext());

        accountSection.setOnClickListener(v -> {
            moveFragment(AccountFragment.this, new AccountUserInfoFragment());
        });

        addressSection.setOnClickListener(v -> {
            moveFragment(AccountFragment.this, new AccountUserAddressFragment());
        });

        logoutSection.setOnClickListener(v -> SessionManager.removeSession());

        return root;
    }

    private void initElements() {

        accountSection = root.findViewById(R.id.accountSection);
        addressSection = root.findViewById(R.id.addressSection);

        logoutSection = root.findViewById(R.id.logoutSection);

    }
}
