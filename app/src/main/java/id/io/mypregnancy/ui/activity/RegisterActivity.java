package id.io.mypregnancy.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.api.model.MobileResponse;
import id.io.mypregnancy.api.model.RegistrationRequest;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.constant.MessageConstant;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.ui.validator.MobileValidator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    Switch approval;

    Button btnRegister;
    EditText txtFirstName, txtLastName, txtEmail, txtPhone, txtPassword, txtConfirmPassword;

    ProgressBar progressBar;
    private MobileValidator validator;

    public RegisterActivity() {
        validator = new MobileValidator();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);
        init();
        approval.setOnClickListener(v -> {
            if (approval.isChecked()) {
                btnRegister.setEnabled(true);
            } else {
                btnRegister.setEnabled(false);
            }
        });


        btnRegister.setOnClickListener(v -> {
            if (btnRegister.isEnabled()) {
                RegistrationRequest request = new RegistrationRequest();
                request.setFullName(txtFirstName.getText().toString() + " " + txtLastName.getText().toString());
                request.setGivenName(txtFirstName.getText().toString());
                request.setEmail(txtEmail.getText().toString());
                request.setPhone(txtPhone.getText().toString());
                request.setPassword(txtPassword.getText().toString());

                if (validator.validateEmail(txtEmail.getText().toString())) {
                    if (validator.validate(request)) {
                        if (txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())) {
                            register(request);
                        } else {
                            showMessage(RegisterActivity.this, MessageConstant.REASON_PASSWORD_VALIDATION);
                        }
                    } else {
                        showMessage(RegisterActivity.this, MessageConstant.REASON_INVALID_REQUEST);
                    }
                } else {
                    showMessage(RegisterActivity.this, MessageConstant.EMAIL_NOT_VALID);
                }
            }
        });

    }

    private void register(RegistrationRequest request) {
        disableElement(true);
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);


        Call<ResponseBody> object = apiInterface.register(JsonHelper.toJson(request));
        object.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        String responseData = response.body().string();

                        MobileResponse user = JsonHelper.fromJson(responseData, MobileResponse.class);
                        Map<String, String> userMap = new HashMap<>();
                        userMap.put(ConstantHelper.KEY_USERID, user.getUserId());
                        userMap.put(ConstantHelper.KEY_EMAIL, user.getEmail());

                        sessionManager(getApplicationContext());
                        SessionManager.setRegisterSession(userMap);
                        movePage(RegisterActivity.this, OtpActivity.class);
                    } else if (response.code() == 302) {
                        showMessage(RegisterActivity.this, MessageConstant.REASON_EMAIL_FOUND);
                    } else if (response.code() == 400) {
                        showMessage(RegisterActivity.this, MessageConstant.REASON_BAD_REQUEST);
                    }

                } catch (IOException e) {
                    showMessage(RegisterActivity.this, e.getMessage());
                }
                disableElement(false);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                showMessage(RegisterActivity.this, t.getMessage());
                disableElement(false);
            }
        });
    }

    private void disableElement(boolean status) {
        if (!status) {
            txtFirstName.setEnabled(true);
            txtLastName.setEnabled(true);
            txtEmail.setEnabled(true);
            txtPhone.setEnabled(true);
            txtPassword.setEnabled(true);
            txtConfirmPassword.setEnabled(true);
            btnRegister.setEnabled(true);
            progressBar.setVisibility(View.GONE);

        } else {
            txtFirstName.setEnabled(false);
            txtLastName.setEnabled(false);
            txtEmail.setEnabled(false);
            txtPhone.setEnabled(false);
            txtPassword.setEnabled(false);
            txtConfirmPassword.setEnabled(false);
            btnRegister.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void init() {

        approval = findViewById(R.id.approval);
        txtFirstName = findViewById(R.id.txtFirstName);
        txtLastName = findViewById(R.id.txtLastName);
        txtEmail = findViewById(R.id.txtEmail);
        txtPhone = findViewById(R.id.txtPhone);
        txtPassword = findViewById(R.id.txtPassword);
        txtConfirmPassword = findViewById(R.id.txtConfirmPassword);
        btnRegister = findViewById(R.id.btnRegister);

        approval.setChecked(false);
        btnRegister.setEnabled(false);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {

        movePage(RegisterActivity.this, GetStartedActivity.class);

    }
}
