package id.io.mypregnancy.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class SplashScreenActivity extends BaseActivity {

    private int LOCATION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splashscreen);

        if (isOnline(getApplicationContext())) {
            if (permissionAlreadyGranted()) {
                serverTest();
            } else {
                requestPermission();
            }

        } else {
            try {
                new AlertDialog.Builder(SplashScreenActivity.this)
                        .setTitle("Error")
                        .setMessage("Internet tidak tersedia, Periksa koneksi internet anda dan coba lagi nanti...")
                        .setCancelable(false)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setNeutralButton("Pengaturan", (dialog, which) ->
                                SplashScreenActivity.this.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS)))
                        .setNegativeButton("Tutup", (dialog, which) -> finish())
                        .show();
            } catch (Exception e) {
                showMessage(SplashScreenActivity.this, e.getMessage());
            }
        }
    }

    private boolean permissionAlreadyGranted() {
        int result = ContextCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this, ACCESS_FINE_LOCATION)) {
        }
        ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                splash();
            } else {
                showMessage(SplashScreenActivity.this, "Permission is denied!");
                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.CAMERA);
                if (!showRationale) {
                    openSettingsDialog();
                }
            }
        }
    }

    private void openSettingsDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SplashScreenActivity.this);
        alertDialogBuilder.setTitle("Application permission");
        alertDialogBuilder
                .setMessage("Anda perlu memberi ijin aplikasi untuk mengakses lokasi anda, buka : Permission > Location > enable location ")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Pengaturan", (dialog, which) -> {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", SplashScreenActivity.this.getPackageName(), null));

                    SplashScreenActivity.this.startActivity(intent);
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void splash() {
        Thread timerTread = new Thread() {
            public void run() {
                try {
                    sleep(4000);
                    movePage(SplashScreenActivity.this, GetStartedActivity.class);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        timerTread.start();
    }

    public void serverTest() {
        APIInterface apiInterface = APIInterface.retrofit.create(APIInterface.class);
        Call<ResponseBody> applicationConfigCall = apiInterface.pingTest();
        applicationConfigCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> configCall, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    splash();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            SplashScreenActivity.this);
                    alertDialogBuilder.setTitle("Layanan tidak tersedia");
                    alertDialogBuilder
                            .setMessage("Sepertinya layanan tidak tersedia saat ini, silahkan coba lagi nanti ..")
                            .setIcon(R.mipmap.ic_launcher)
                            .setCancelable(false)
                            .setNegativeButton("Tutup", (dialog, which) -> finish());
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> failCall, Throwable t) {
                showMessage(getApplicationContext(), t.getMessage());
            }
        });

    }
}
