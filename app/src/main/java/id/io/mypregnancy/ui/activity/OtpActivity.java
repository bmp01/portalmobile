package id.io.mypregnancy.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import id.io.mypregnancy.R;
import id.io.mypregnancy.api.APIInterface;
import id.io.mypregnancy.api.model.OTPRequest;
import id.io.mypregnancy.api.model.ServiceResponse;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.constant.ErrorReasonConstant;
import id.io.mypregnancy.constant.MessageConstant;
import id.io.mypregnancy.helper.json.JsonHelper;
import id.io.mypregnancy.manager.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends BaseActivity {

    public int counter;
    String counterText;

    Button validateOtp;
    TextView txtOtp, txtEmailInfo, txtResend, txtCountInfo, txtCount;
    ProgressBar loading_progress;

    Map<String, String> sessionMap = new HashMap<>();
    APIInterface apiInterface;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_otp);

        sessionManager(getApplicationContext());
        sessionMap = SessionManager.getRegisterSession();
        initElement();

        counterText = txtCount.getText().toString();

        new CountDownTimer(50000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                txtCount.setText(String.valueOf(counter));
                counter++;
            }

            @Override
            public void onFinish() {
                txtCount.setText("Finished");
                txtCountInfo.setVisibility(View.INVISIBLE);
                txtCount.setVisibility(View.INVISIBLE);

                txtResend.setTextColor(Color.BLUE);
                txtResend.setEnabled(true);
            }
        }.start();
        txtResend.setOnClickListener(v -> {
            if (txtResend.isEnabled()) {
                OTPRequest request = new OTPRequest(sessionMap.get(ConstantHelper.KEY_EMAIL));
                resendOtp(request);
            }
        });

        validateOtp.setOnClickListener(v -> {
            validateOtp(txtOtp.getText().toString());
        });
    }

    public void validateOtp(String otp) {
        validateOtp.setEnabled(false);
        loading_progress.setVisibility(View.VISIBLE);

        Call<ResponseBody> object = apiInterface.validateOtp(otp);
        object.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        sessionManager(getApplicationContext());
                        Map<String, String> userMap = SessionManager.getRegisterSession();
                        SessionManager.setLoginSession(userMap);
                        SessionManager.setUserIdSession(userMap.get(ConstantHelper.KEY_USERID));
                        SessionManager.removeRegistrationSession();

                        movePage(OtpActivity.this, MainActivity.class);
                    } else if (response.code() == 406) {
                        String responseData = response.errorBody().string();
                        ServiceResponse serviceResponse = JsonHelper.fromJson(responseData, ServiceResponse.class);
                        if (serviceResponse.getDescription().contains("otp_expired")) {
                            showMessage(OtpActivity.this, ErrorReasonConstant.REASON_EXPIRED_OTP_CODE);
                        } else if (serviceResponse.getDescription().contains("invalid_otp")) {
                            showMessage(OtpActivity.this, ErrorReasonConstant.REASON_INVALID_OTP_CODE);
                        }
                    }
                } catch (IOException e) {
                    showMessage(OtpActivity.this, e.getMessage());
                }
                loading_progress.setVisibility(View.GONE);
                validateOtp.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showMessage(OtpActivity.this, t.getMessage());
                loading_progress.setVisibility(View.GONE);
                validateOtp.setEnabled(true);
            }
        });
    }

    public void resendOtp(OTPRequest request) {
        loading_progress.setVisibility(View.VISIBLE);
        validateOtp.setEnabled(false);
        Call<ResponseBody> object = apiInterface.resendOtp(JsonHelper.toJson(request));
        object.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200) {
                    showMessage(OtpActivity.this, MessageConstant.MESSAGE_OTP_SEND + sessionMap.get(ConstantHelper.KEY_EMAIL));

                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);

                } else if (response.code() == 406) {
                    showMessage(OtpActivity.this, MessageConstant.REASON_EMAIL_NOT_FOUND);
                }
                loading_progress.setVisibility(View.GONE);
                validateOtp.setEnabled(true);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                showMessage(OtpActivity.this, t.getMessage());
                loading_progress.setVisibility(View.GONE);
                validateOtp.setEnabled(true);
            }
        });
    }

    public void initElement() {

        apiInterface = APIInterface.retrofit.create(APIInterface.class);

        loading_progress = (ProgressBar) findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);

        txtEmailInfo = (TextView) findViewById(R.id.txtEmailInfo);
        txtResend = (TextView) findViewById(R.id.txtResend);
        txtCount = (TextView) findViewById(R.id.txtCount);
        txtCountInfo = (TextView) findViewById(R.id.txtCountInfo);
        validateOtp = (Button) findViewById(R.id.validateOtp);
        txtOtp = (TextView) findViewById(R.id.txtOtp);

        txtResend.setTextColor(Color.GRAY);
        txtResend.setEnabled(false);

        txtCountInfo.setVisibility(View.VISIBLE);
        txtCount.setVisibility(View.VISIBLE);

        counterText = txtCount.getText().toString();

        String emailInfo = txtEmailInfo.getText().toString();
        emailInfo = emailInfo.replaceAll(ConstantHelper.REGEX_EMAIL, sessionMap.get(ConstantHelper.KEY_EMAIL));
        txtEmailInfo.setText(emailInfo);

    }
}
