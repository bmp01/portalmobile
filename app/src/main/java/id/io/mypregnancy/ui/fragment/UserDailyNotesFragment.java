package id.io.mypregnancy.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import id.io.mypregnancy.R;

public class UserDailyNotesFragment extends BaseFragment {
    //root elements
    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_user_daily_notes, container, false);

        return root;
    }
}
