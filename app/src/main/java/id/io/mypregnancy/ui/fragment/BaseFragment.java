package id.io.mypregnancy.ui.fragment;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.io.mypregnancy.R;
import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.helper.IntentHelper;
import id.io.mypregnancy.manager.SessionManager;
import id.io.mypregnancy.model.User;
import id.io.mypregnancy.model.UserGestation;
import id.io.mypregnancy.ui.custom.CustomScrollView;

public class BaseFragment extends Fragment {

    private static IntentHelper intentHelper = new IntentHelper();

    protected static String cacheUserId;
    protected static String hplDate;

    protected static void movePage(Context ctx, Class target) {
        intentHelper.movePage(ctx, target);
    }

    protected static SessionManager sessionManager(Context context) {
        SessionManager sessionManager = new SessionManager(context);
        if (!SessionManager.hasLogin()) {
            SessionManager.checkLoginSession();
        }
        return sessionManager;
    }

    protected static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    protected static void reloadActivity(Fragment fragment) {
        FragmentTransaction ft = fragment.getFragmentManager().beginTransaction();
        ft.detach(fragment).attach(fragment).commit();

    }

    protected static boolean validateJson(Map<String, String> map, String values) {
        if (map.get(values) != null) {
            return true;
        }
        return false;
    }

    public void loadPage(boolean states, ProgressBar progressBar, CustomScrollView customScrollView, RelativeLayout relativeLayout) {
        if (states) {
            progressBar.setVisibility(View.VISIBLE);
            customScrollView.setEnableScrolling(false);
            relativeLayout.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            customScrollView.setEnableScrolling(true);
            relativeLayout.setVisibility(View.GONE);
        }
    }

    protected static void moveFragment(Fragment oldfragment, Fragment newFragment) {
        FragmentManager fm = oldfragment.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.nav_host_fragment, newFragment);
        ft.commit();
    }

    protected static ArrayAdapter<CharSequence> generateDropdown(Context context, int arrayRes) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, arrayRes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    protected static ArrayAdapter<String> generateDropdown(Context context, int resource, List<String> list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, resource, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    protected static void renewSession() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar currentTimeNow = Calendar.getInstance();

        Date expiry = dateFormat.parse(SessionManager.getRefreshTime());
        Calendar expiryDt = Calendar.getInstance();
        expiryDt.setTime(expiry);

        if (expiry.before(currentTimeNow.getTime())) {
            currentTimeNow.add(Calendar.MINUTE, ConstantHelper.SESSION_EXPIRY);
            SessionManager.setRefreshTime(dateFormat.format(currentTimeNow.getTime()));

            SessionManager.removeUserSession();
            SessionManager.removeUserAddressSession();
            SessionManager.removeGestationSession();


/*            SessionManager.revokeUserSession();
            SessionManager.revokeUserLocationSession();
            SessionManager.revokeUserGestationSession();
            SessionManager.revokeGestationSession();*/
        }
    }

    protected static Map<String, String> parseUserToMap(User user) {
        Map<String, String> userMap = new HashMap<>();
        userMap.put(ConstantHelper.KEY_USERID, user.getUserId());
        userMap.put(ConstantHelper.KEY_CARD_ID, user.getCardId());
        userMap.put(ConstantHelper.KEY_FULLNAME, user.getFullName());
        userMap.put(ConstantHelper.KEY_GIVENNAME, user.getGivenName());
        userMap.put(ConstantHelper.KEY_EMAIL, user.getEmail());
        userMap.put(ConstantHelper.KEY_PHONE, user.getPhone());
        userMap.put(ConstantHelper.KEY_RELATION, user.getRelation());
        userMap.put(ConstantHelper.KEY_AGES, user.getAge());

        return userMap;
    }

    protected static Map<String, String> parseGestationToTrackingMap(UserGestation gestation) {
        Map<String, String> gestationMap = new HashMap<>();
        gestationMap.put(ConstantHelper.KEY_GESTATION_ID, gestation.getId());
        gestationMap.put(ConstantHelper.KEY_GESTATION_INSEMINATION, gestation.getInseminationDt());
        gestationMap.put(ConstantHelper.KEY_GESTATION_HPL, gestation.getDueDate());

        return gestationMap;
    }
}
