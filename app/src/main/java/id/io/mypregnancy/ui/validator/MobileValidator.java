package id.io.mypregnancy.ui.validator;

import android.text.TextUtils;
import android.util.Patterns;

import id.io.mypregnancy.api.model.AuthenticationRequest;
import id.io.mypregnancy.api.model.RegistrationRequest;

public class MobileValidator extends BaseValidator {

    public MobileValidator() {
        // Empty Constructor
    }

    public boolean validateEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean validate(String str) {
        return notNull(str);
    }

    public boolean validate(AuthenticationRequest request) {
        return notNull(request) && notNull(request.getEmail()) && notNull(request.getPassword());
    }

    public boolean validate(RegistrationRequest request) {
        return notNull(request) && notNull(request.getFullName()) && notNull(request.getGivenName())
                && notNull(request.getEmail()) && notNull(request.getPhone()) && notNull(request.getPassword());
    }


}
