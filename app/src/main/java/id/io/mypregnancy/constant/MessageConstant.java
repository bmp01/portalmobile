package id.io.mypregnancy.constant;

public class MessageConstant {

    // public static String MESSAGE = "field";
    public static final String REASON_EMPTY_EMAIL_AND_PASSWORD_FIELD = "email dan/atau password kosong";
    public static final String EMAIL_NOT_VALID = "email tidak valid!";
    public static final String REASON_EMPTY_EMAIL_FIELD = "masukkan email!";
    public static final String REASON_EMPTY_PASSWORD_FIELD = "masukkan password!";
    public static final String REASON_INVALID_USERNAME_AND_OR_PASSWORD = "Email dan/atau password salah";
    public static final String REASON_ON_OTP_VALIDATION = "Akun perlu validasi";
    public static final String REASON_EMAIL_NOT_FOUND = "Email tidak terdaftar";
    public static final String REASON_EMAIL_FOUND = "Email sudah terdaftar";

    public static final String REASON_INVALID_REQUEST = "Data tidak lengkap!";
    public static final String REASON_PASSWORD_VALIDATION = "Password tidak sama";

    public static final String REASON_BAD_REQUEST = "Permintaan tidak lengkap";


    public static String MESSAGE_OTP_SEND = "Kode OTP terkirim ke ";

    public static String MESSAGE_GESTATION_DESCRIPTION = "Anda sedang hamil %weeks% minggu dan %days% hari";
    public static String MESSAGE_GESTATION_DUE_DATE = "perkiraan hari persalinan : %dueDate%";
    public static String MESSAGE_GESTATION_DATE_REMAINING = "%weeks% minggu dan %days% hari tersisa";

    public static String MESSAGE_DETAILS_HAS_CHANGED = "Detail user disimpan";

    public static String MESSAGE_MISBIRTH = "Kami turut bersedih atas keguguran anda";
    public static String MESSAGE_BEENBORN = "Kami ucapkan, Selamat atas kelahiran putra/putrinya";
}
