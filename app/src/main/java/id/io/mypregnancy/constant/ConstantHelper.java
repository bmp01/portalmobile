package id.io.mypregnancy.constant;

public class ConstantHelper {

    //Global configuration
    public static final String KEY_SESSION_TIMESTAMP = "session-timestamp";
    public static final int SESSION_EXPIRY = 2;//minutes

    public static final String KEY_USERID = "user-id";
    public static final String KEY_ID = "id";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_STATUS = "status";
    public static final String KEY_NAME = "name";
    public static final String KEY_CREATE_DATE = "create-dt";


    //New user configuration
    public static final String KEY_CARD_ID = "card-id";
    public static final String KEY_GIVENNAME = "given-name";
    public static final String KEY_FULLNAME = "full-name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_PHONE = "phone";

    // Existing / updated user configuration
    public static final String KEY_RELATION = "who-am-i";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_AGES = "age";
    public static final String KEY_AVATAR = "avatar";

    // User Address Configuration
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PROVINCE = "province";
    public static final String KEY_REGENCY = "regency";
    public static final String KEY_DISTRICT = "district";
    public static final String KEY_VILLAGE = "village";
    public static final String KEY_POSTAL_CODE = "postal-code";


    //User gestation Configuration
    public static final String KEY_GESTATION_ID = "gestation-id";
    public static final String KEY_GESTATION_NAME = "baby-name";
    public static final String KEY_GESTATION_GENDER = "baby-gender";
    public static final String KEY_GESTATION_INSEMINATION = "insemination-dt";
    public static final String KEY_GESTATION_HPL = "hpl-dt";
    public static final String KEY_GESTATION_FIRST_GESTATION = "first-gestation";
    public static final String KEY_GESTATION_BEEN_BORN = "been-born";
    public static final String KEY_GESTATION_MIS_BIRTH = "mis-birth";

    //User schedule Configuration
    public static final String KEY_SCHEDULE_ID = "schedule-id";
    public static final String KEY_SCHEDULE_NAME = "schedule-name";
    public static final String KEY_SCHEDULE_DATE = "appointment-date";
    public static final String KEY_SCHEDULE_TIME = "appointment-time";

    public static final String KEY_CAL_HPL = "calculater";

    public static final String KEY_CLOSE = "Tutup";

    //OTP Configuration
    public static final String OTP_CODE = "otp";

    //Regex configuration
    public static final String REGEX_EMAIL = "%user@mail.com%";
    public static final String REGEX_COUNT = "%00%";


}
