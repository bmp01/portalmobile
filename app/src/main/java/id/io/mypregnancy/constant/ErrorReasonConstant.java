package id.io.mypregnancy.constant;

public class ErrorReasonConstant {


    public static final String REASON_EXPIRED_OTP_CODE = "Kode OTP kadaluarsa";
    public static final String REASON_INVALID_OTP_CODE = "Kode OTP salah";

    // Error Servers config
    public static final String REASON_SERVER_NOT_FOUND = "Server tidak ditemukan";
    public static final String REASON_SERVER_UNAVAILABLE = "Kesalahan system";

    public static final String REASON_FAILED_USER_UPDATE = "Gagal update data user!";

}
