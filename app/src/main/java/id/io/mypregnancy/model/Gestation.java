package id.io.mypregnancy.model;

public class Gestation {

    private String userId;
    private int id;
    private String babyGender;
    private String babyName;
    private String dueDate;
    private String insemination;
    private boolean firstPregnant;
    private boolean beenBorn;
    private boolean missBirth;

    public Gestation() {
    }

    public Gestation(String userId, int id, String babyGender, String babyName, String dueDate, String insemination, boolean firstPregnant, boolean beenBorn, boolean missBirth) {
        this.userId = userId;
        this.id = id;
        this.babyGender = babyGender;
        this.babyName = babyName;
        this.dueDate = dueDate;
        this.firstPregnant = firstPregnant;
        this.beenBorn = beenBorn;
        this.missBirth = missBirth;
        this.insemination = insemination;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBabyGender() {
        return babyGender;
    }

    public void setBabyGender(String babyGender) {
        this.babyGender = babyGender;
    }

    public String getBabyName() {
        return babyName;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isFirstPregnant() {
        return firstPregnant;
    }

    public void setFirstPregnant(boolean firstPregnant) {
        this.firstPregnant = firstPregnant;
    }

    public boolean isBeenBorn() {
        return beenBorn;
    }

    public void setBeenBorn(boolean beenBorn) {
        this.beenBorn = beenBorn;
    }

    public boolean isMissBirth() {
        return missBirth;
    }

    public void setMissBirth(boolean missBirth) {
        this.missBirth = missBirth;
    }

    public String getInsemination() {
        return insemination;
    }

    public void setInsemination(String insemination) {
        this.insemination = insemination;
    }
}
