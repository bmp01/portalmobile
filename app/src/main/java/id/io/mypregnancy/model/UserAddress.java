package id.io.mypregnancy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.io.mypregnancy.constant.ConstantHelper;

public class UserAddress {

    @JsonProperty(ConstantHelper.KEY_USERID)
    private String userId;
    @JsonProperty(ConstantHelper.KEY_PROVINCE)
    private String province;
    @JsonProperty(ConstantHelper.KEY_DISTRICT)
    private String district;
    @JsonProperty(ConstantHelper.KEY_REGENCY)
    private String regency;
    @JsonProperty(ConstantHelper.KEY_VILLAGE)
    private String village;
    @JsonProperty(ConstantHelper.KEY_POSTAL_CODE)
    private String postalCode;
    @JsonProperty(ConstantHelper.KEY_ADDRESS)
    private String address;
    @JsonProperty(ConstantHelper.KEY_LATITUDE)
    private String latitude;
    @JsonProperty(ConstantHelper.KEY_LONGITUDE)
    private String longitude;

    public UserAddress() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRegency() {
        return regency;
    }

    public void setRegency(String regency) {
        this.regency = regency;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
