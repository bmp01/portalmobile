package id.io.mypregnancy.model;

public class GestationCard {

    private String babyName;
    private String babyGender;
    private String babyHpl;
    private String status;

    public GestationCard() {
    }

    public GestationCard(String babyName, String babyGender, String babyHpl, String status) {
        this.babyName = babyName;
        this.babyGender = babyGender;
        this.babyHpl = babyHpl;
        this.status = status;

    }

    public String getBabyName() {
        return babyName;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }

    public String getBabyGender() {
        return babyGender;
    }

    public void setBabyGender(String babyGender) {
        this.babyGender = babyGender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBabyHpl() {
        return babyHpl;
    }

    public void setBabyHpl(String babyHpl) {
        this.babyHpl = babyHpl;
    }
}
