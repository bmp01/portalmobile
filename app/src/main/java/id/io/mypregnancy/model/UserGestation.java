package id.io.mypregnancy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.io.mypregnancy.constant.ConstantHelper;

public class UserGestation {

    @JsonProperty(ConstantHelper.KEY_ID)
    private String id;

    @JsonProperty(ConstantHelper.KEY_NAME)
    private String name;

    @JsonProperty(ConstantHelper.KEY_GENDER)
    private String gender;

    @JsonProperty(ConstantHelper.KEY_GESTATION_INSEMINATION)
    private String inseminationDt;

    @JsonProperty(ConstantHelper.KEY_GESTATION_HPL)
    private String dueDate;

    @JsonProperty(ConstantHelper.KEY_GESTATION_FIRST_GESTATION)
    private boolean first;

    @JsonProperty(ConstantHelper.KEY_GESTATION_BEEN_BORN)
    private boolean born;

    @JsonProperty(ConstantHelper.KEY_GESTATION_MIS_BIRTH)
    private boolean missBirth;

    @JsonProperty(ConstantHelper.KEY_CREATE_DATE)
    private String createDt;

    public UserGestation() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInseminationDt() {
        return inseminationDt;
    }

    public void setInseminationDt(String inseminationDt) {
        this.inseminationDt = inseminationDt;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isBorn() {
        return born;
    }

    public void setBorn(boolean born) {
        this.born = born;
    }

    public boolean isMissBirth() {
        return missBirth;
    }

    public void setMissBirth(boolean missBirth) {
        this.missBirth = missBirth;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }
}
