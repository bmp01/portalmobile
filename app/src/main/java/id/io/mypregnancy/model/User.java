package id.io.mypregnancy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.io.mypregnancy.constant.ConstantHelper;

public class User {

    @JsonProperty(ConstantHelper.KEY_USERID)
    private String userId;
    @JsonProperty(ConstantHelper.KEY_CARD_ID)
    private String cardId;
    @JsonProperty(ConstantHelper.KEY_FULLNAME)
    private String fullName;
    @JsonProperty(ConstantHelper.KEY_GIVENNAME)
    private String givenName;
    @JsonProperty(ConstantHelper.KEY_EMAIL)
    private String email;
    @JsonProperty(ConstantHelper.KEY_PHONE)
    private String phone;
    @JsonProperty(ConstantHelper.KEY_RELATION)
    private String relation;
    @JsonProperty(ConstantHelper.KEY_AGES)
    private String age;

    public User() {
        // Empty constructor
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

}
