package id.io.mypregnancy.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;
import java.util.Map;

import id.io.mypregnancy.constant.ConstantHelper;
import id.io.mypregnancy.model.User;
import id.io.mypregnancy.model.UserAddress;
import id.io.mypregnancy.model.UserGestation;
import id.io.mypregnancy.service.BaseService;
import id.io.mypregnancy.ui.activity.LoginActivity;

@SuppressLint("CommitPrefEdits")
public class SessionManager extends BaseService {

    private static Context _context;
    int PRIVATE_MODE = 0;

    private static final String SESSION_LOGIN = "portal_login";
    private static final String STATUS = "status";
    private static final String REGISTER_SESSION = "register-session";
    private static final String USER_SESSION = "user-session";
    private static final String SESSION_REFRESH_TIME = "refresh-time-session";
    private static final String SESSION_USER_ID = "userid-session";
    private static final String SESSION_USER_ADDRESS = "session-address";
    private static final String SESSION_USER_ADDRESS_CACHE = "session-address-cache";
    private static final String GESTATION_SESSION = "gestation-session";

    private static SharedPreferences loginPreferences;
    private static SharedPreferences registerPreferences;
    private static SharedPreferences userIdPreferences;
    private static SharedPreferences refreshTimePreferences;
    private static SharedPreferences userPreferences;
    private static SharedPreferences userAddressPreferences;
    private static SharedPreferences userAddressCachePreferences;
    private static SharedPreferences gestationPreferences;

    private static Editor loginEditor, registrationEditor, userIdEditor, refreshTimeEditor,
            userEditor, userAddressEditor, userAddressCacheEditor, gestationEditor;

    public SessionManager(Context context) {
        this._context = context;

        loginPreferences = _context.getSharedPreferences(SESSION_LOGIN, PRIVATE_MODE);
        registerPreferences = _context.getSharedPreferences(REGISTER_SESSION, PRIVATE_MODE);
        userIdPreferences = _context.getSharedPreferences(SESSION_USER_ID, PRIVATE_MODE);
        refreshTimePreferences = _context.getSharedPreferences(SESSION_REFRESH_TIME, PRIVATE_MODE);
        userPreferences = _context.getSharedPreferences(USER_SESSION, PRIVATE_MODE);
        userAddressPreferences = _context.getSharedPreferences(SESSION_USER_ADDRESS, PRIVATE_MODE);
        userAddressCachePreferences = _context.getSharedPreferences(SESSION_USER_ADDRESS_CACHE, PRIVATE_MODE);
        gestationPreferences = _context.getSharedPreferences(GESTATION_SESSION, PRIVATE_MODE);

        loginEditor = loginPreferences.edit();
        registrationEditor = registerPreferences.edit();
        userEditor = userPreferences.edit();
        userIdEditor = userIdPreferences.edit();
        refreshTimeEditor = refreshTimePreferences.edit();
        userAddressEditor = userAddressPreferences.edit();
        userAddressCacheEditor = userAddressCachePreferences.edit();
        gestationEditor = gestationPreferences.edit();

    }

    public static boolean hasLogin() {
        return loginPreferences.getBoolean(STATUS, false);
    }

    public static void checkLoginSession() {
        if (hasLogin()) {
            loginEditor.putBoolean(STATUS, false);
            removeSession();
        }
    }

    public static Map<String, String> getLoginSession() {
        Map<String, String> session = new HashMap<>();

        session.put(ConstantHelper.KEY_USERID, loginPreferences.getString(ConstantHelper.KEY_USERID, null));
        return session;

    }

    /**
     * LOGIN SESSION
     */
    public static void setLoginSession(Map<String, String> sessionMap) {
        loginEditor.putBoolean(STATUS, true);
        if (sessionMap.containsKey(ConstantHelper.KEY_USERID)) {
            loginEditor.putString(ConstantHelper.KEY_USERID, sessionMap.get(ConstantHelper.KEY_USERID));
        }
        if (sessionMap.containsKey(ConstantHelper.KEY_EMAIL)) {
            loginEditor.putString(ConstantHelper.KEY_EMAIL, sessionMap.get(ConstantHelper.KEY_EMAIL));
        }
        loginEditor.commit();
    }

    public static Map<String, String> getRegisterSession() {

        Map<String, String> session = new HashMap<>();

        session.put(ConstantHelper.KEY_USERID, registerPreferences.getString(ConstantHelper.KEY_USERID, null));
        session.put(ConstantHelper.KEY_EMAIL, registerPreferences.getString(ConstantHelper.KEY_EMAIL, null));

        return session;
    }

    /**
     * REGISTER SESSION
     */
    public static void setRegisterSession(Map<String, String> sessionMap) {
        registrationEditor.putBoolean(REGISTER_SESSION, true);
        if (sessionMap.containsKey(ConstantHelper.KEY_USERID)) {
            registrationEditor.putString(ConstantHelper.KEY_USERID, sessionMap.get(ConstantHelper.KEY_USERID));
        }
        if (sessionMap.containsKey(ConstantHelper.KEY_EMAIL)) {
            registrationEditor.putString(ConstantHelper.KEY_EMAIL, sessionMap.get(ConstantHelper.KEY_EMAIL));
        }
        registrationEditor.commit();
    }

    /**
     * REFRESH TIME
     */
    public static String getRefreshTime() {
        String result = refreshTimePreferences.getString(ConstantHelper.KEY_SESSION_TIMESTAMP, null);
        return result;
    }

    public static void setRefreshTime(String date) {
        refreshTimeEditor.putBoolean(SESSION_REFRESH_TIME, true);

        refreshTimeEditor.putString(ConstantHelper.KEY_SESSION_TIMESTAMP, date);

        refreshTimeEditor.commit();
    }

    public static boolean hasRefreshTime() {
        return refreshTimePreferences.getBoolean(SESSION_REFRESH_TIME, false);
    }

    /**
     * USER ID SESSION
     */
    public static String getUserIdSession() {
        String result = userIdPreferences.getString(ConstantHelper.KEY_USERID, null);
        return result;
    }

    public static void setUserIdSession(String userId) {
        userIdEditor.putBoolean(SESSION_USER_ID, true);
        userIdEditor.putString(ConstantHelper.KEY_USERID, userId);
        userIdEditor.commit();
    }

    public static UserAddress getUserAddressCacheSession() {
        UserAddress cache = new UserAddress();

        cache.setUserId(userAddressCachePreferences.getString(ConstantHelper.KEY_USERID, null));
        cache.setAddress(userAddressCachePreferences.getString(ConstantHelper.KEY_ADDRESS, null));
        cache.setProvince(userAddressCachePreferences.getString(ConstantHelper.KEY_PROVINCE, null));
        cache.setDistrict(userAddressCachePreferences.getString(ConstantHelper.KEY_DISTRICT, null));
        cache.setRegency(userAddressCachePreferences.getString(ConstantHelper.KEY_REGENCY, null));
        cache.setVillage(userAddressCachePreferences.getString(ConstantHelper.KEY_VILLAGE, null));
        cache.setPostalCode(userAddressCachePreferences.getString(ConstantHelper.KEY_POSTAL_CODE, null));
        cache.setLatitude(userAddressCachePreferences.getString(ConstantHelper.KEY_LATITUDE, null));
        cache.setLongitude(userAddressCachePreferences.getString(ConstantHelper.KEY_LONGITUDE, null));

        return cache;
    }

    /**
     * ADDRESS SESSION
     */
    public static void setUserAddressCacheSession(UserAddress request) {
        userAddressCacheEditor.putBoolean(SESSION_USER_ADDRESS_CACHE, true);
        userAddressCacheEditor.putString(ConstantHelper.KEY_USERID, request.getUserId());
        userAddressCacheEditor.putString(ConstantHelper.KEY_ADDRESS, request.getAddress());
        userAddressCacheEditor.putString(ConstantHelper.KEY_PROVINCE, request.getProvince());
        userAddressCacheEditor.putString(ConstantHelper.KEY_DISTRICT, request.getDistrict());
        userAddressCacheEditor.putString(ConstantHelper.KEY_REGENCY, request.getRegency());
        userAddressCacheEditor.putString(ConstantHelper.KEY_VILLAGE, request.getVillage());
        userAddressCacheEditor.putString(ConstantHelper.KEY_POSTAL_CODE, request.getPostalCode());
        userAddressCacheEditor.putString(ConstantHelper.KEY_LATITUDE, request.getLatitude());
        userAddressCacheEditor.putString(ConstantHelper.KEY_LONGITUDE, request.getLongitude());
        userAddressCacheEditor.commit();
    }

    public static boolean hasUserAddressCacheSession() {
        return userAddressCachePreferences.getBoolean(SESSION_USER_ADDRESS_CACHE, false);
    }

    public static UserAddress getUserAddressSession() {
        UserAddress session = new UserAddress();

        session.setUserId(userAddressPreferences.getString(ConstantHelper.KEY_USERID, null));
        session.setAddress(userAddressPreferences.getString(ConstantHelper.KEY_ADDRESS, null));
        session.setProvince(userAddressPreferences.getString(ConstantHelper.KEY_PROVINCE, null));
        session.setDistrict(userAddressPreferences.getString(ConstantHelper.KEY_DISTRICT, null));
        session.setRegency(userAddressPreferences.getString(ConstantHelper.KEY_REGENCY, null));
        session.setVillage(userAddressPreferences.getString(ConstantHelper.KEY_VILLAGE, null));
        session.setPostalCode(userAddressPreferences.getString(ConstantHelper.KEY_POSTAL_CODE, null));
        session.setLatitude(userAddressPreferences.getString(ConstantHelper.KEY_LATITUDE, null));
        session.setLongitude(userAddressPreferences.getString(ConstantHelper.KEY_LONGITUDE, null));

        return session;
    }

    public static void setUserAddressSession(UserAddress session) {
        userAddressEditor.putBoolean(SESSION_USER_ADDRESS, true);
        userAddressEditor.putString(ConstantHelper.KEY_USERID, session.getUserId());
        userAddressEditor.putString(ConstantHelper.KEY_USERID, session.getUserId());
        userAddressEditor.putString(ConstantHelper.KEY_ADDRESS, session.getAddress());
        userAddressEditor.putString(ConstantHelper.KEY_PROVINCE, session.getProvince());
        userAddressEditor.putString(ConstantHelper.KEY_DISTRICT, session.getDistrict());
        userAddressEditor.putString(ConstantHelper.KEY_REGENCY, session.getRegency());
        userAddressEditor.putString(ConstantHelper.KEY_VILLAGE, session.getVillage());
        userAddressEditor.putString(ConstantHelper.KEY_POSTAL_CODE, session.getPostalCode());
        userAddressEditor.putString(ConstantHelper.KEY_LATITUDE, session.getLatitude());
        userAddressEditor.putString(ConstantHelper.KEY_LONGITUDE, session.getLongitude());

        userAddressEditor.commit();
    }

    public static boolean hasUserAddressSession() {
        return userAddressPreferences.getBoolean(SESSION_USER_ADDRESS, false);
    }

    public static boolean isUserSession() {
        return userPreferences.getBoolean(USER_SESSION, false);
    }

    public static User getUserSession() {
        User session = new User();
        session.setUserId(userPreferences.getString(ConstantHelper.KEY_USERID, null));
        session.setCardId(userPreferences.getString(ConstantHelper.KEY_CARD_ID, null));
        session.setFullName(userPreferences.getString(ConstantHelper.KEY_FULLNAME, null));
        session.setGivenName(userPreferences.getString(ConstantHelper.KEY_GIVENNAME, null));
        session.setEmail(userPreferences.getString(ConstantHelper.KEY_EMAIL, null));
        session.setPhone(userPreferences.getString(ConstantHelper.KEY_PHONE, null));
        session.setRelation(userPreferences.getString(ConstantHelper.KEY_RELATION, null));
        session.setAge(userPreferences.getString(ConstantHelper.KEY_AGES, null));

        return session;
    }

    /**
     * USER SESSION
     */
    public static void setUserSession(User user) {
        userEditor.putBoolean(USER_SESSION, true);
        userEditor.putString(ConstantHelper.KEY_USERID, user.getUserId());
        userEditor.putString(ConstantHelper.KEY_CARD_ID, user.getCardId());
        userEditor.putString(ConstantHelper.KEY_FULLNAME, user.getFullName());
        userEditor.putString(ConstantHelper.KEY_GIVENNAME, user.getGivenName());
        userEditor.putString(ConstantHelper.KEY_EMAIL, user.getEmail());
        userEditor.putString(ConstantHelper.KEY_PHONE, user.getPhone());
        userEditor.putString(ConstantHelper.KEY_RELATION, user.getRelation());
        userEditor.putString(ConstantHelper.KEY_AGES, user.getAge());

        userEditor.commit();
    }

    public static boolean isGestationSession() {
        return gestationPreferences.getBoolean(GESTATION_SESSION, false);
    }

    public static UserGestation getGestationSession() {
        UserGestation session = new UserGestation();
        session.setId(gestationPreferences.getString(ConstantHelper.KEY_ID, null));
        session.setName(gestationPreferences.getString(ConstantHelper.KEY_GESTATION_NAME, null));
        session.setGender(gestationPreferences.getString(ConstantHelper.KEY_GESTATION_GENDER, null));
        session.setInseminationDt(gestationPreferences.getString(ConstantHelper.KEY_GESTATION_INSEMINATION, null));
        session.setDueDate(gestationPreferences.getString(ConstantHelper.KEY_GESTATION_HPL, null));
        session.setFirst(gestationPreferences.getBoolean(ConstantHelper.KEY_GESTATION_FIRST_GESTATION, false));
        session.setBorn(gestationPreferences.getBoolean(ConstantHelper.KEY_GESTATION_BEEN_BORN, false));
        session.setMissBirth(gestationPreferences.getBoolean(ConstantHelper.KEY_GESTATION_MIS_BIRTH, false));
        session.setCreateDt(gestationPreferences.getString(ConstantHelper.KEY_CREATE_DATE, null));

        return session;
    }

    /**
     * GESTATION SESSION
     */
    public static void setGestationSession(UserGestation content) {
        gestationEditor.putBoolean(GESTATION_SESSION, true);

        if (!content.getId().isEmpty()) {
            gestationEditor.putString(ConstantHelper.KEY_ID, content.getId());
        }

        if (!content.getName().isEmpty()) {
            gestationEditor.putString(ConstantHelper.KEY_NAME, content.getName());
        }

        if (!content.getGender().isEmpty()) {
            gestationEditor.putString(ConstantHelper.KEY_GENDER, content.getGender());
        }

        if (!content.getInseminationDt().isEmpty()) {
            gestationEditor.putString(ConstantHelper.KEY_GESTATION_INSEMINATION, content.getInseminationDt());
        }
        if (!content.getDueDate().isEmpty()) {
            gestationEditor.putString(ConstantHelper.KEY_GESTATION_HPL, content.getDueDate());
        }
        if (!content.getCreateDt().isEmpty()) {
            gestationEditor.putString(ConstantHelper.KEY_CREATE_DATE, content.getCreateDt());
        }
        gestationEditor.putBoolean(ConstantHelper.KEY_GESTATION_FIRST_GESTATION, content.isFirst());
        gestationEditor.putBoolean(ConstantHelper.KEY_GESTATION_BEEN_BORN, content.isBorn());
        gestationEditor.putBoolean(ConstantHelper.KEY_GESTATION_MIS_BIRTH, content.isMissBirth());

        gestationEditor.commit();
    }

    public static void removeSession() {

/*        private static Editor loginEditor, registrationEditor, userIdEditor, refreshTimeEditor,
                userEditor, userAddressEditor, userAddressCacheEditor,gestationEditor;*/

        removeLoginSession();
        removeRegistrationSession();
        removeUserIdSession();
        removeRefreshTimeSession();
        removeUserSession();
        removeUserAddressSession();
        removeUserAddressCacheSession();
        removeGestationSession();

        movePage(_context, LoginActivity.class);
    }

    public static void removeLoginSession() {
        loginEditor.clear();
        loginEditor.commit();
    }

    public static void removeRegistrationSession() {
        registrationEditor.clear();
        registrationEditor.commit();
    }

    public static void removeUserIdSession() {
        userIdEditor.clear();
        userIdEditor.commit();
    }

    public static void removeRefreshTimeSession() {
        refreshTimeEditor.clear();
        refreshTimeEditor.commit();
    }

    public static void removeUserSession() {
        userEditor.clear();
        userEditor.commit();
    }

    public static void removeUserAddressSession() {
        userAddressEditor.clear();
        userAddressEditor.commit();
    }

    public static void removeUserAddressCacheSession() {
        userAddressCacheEditor.clear();
        userAddressCacheEditor.commit();
    }

    public static void removeGestationSession() {
        gestationEditor.clear();
        gestationEditor.commit();
    }

}
